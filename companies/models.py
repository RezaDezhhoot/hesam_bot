from django.db import models


class Companieslink(models.Model):
    badge = models.CharField(null=False, blank=False, max_length=255, verbose_name="نماد")
    link = models.URLField(null=False, blank=False, verbose_name="لینک")

    def __str__(self):
        return self.link

    class Meta:
        verbose_name = "کمپانی ها"
        verbose_name_plural = "کمپانی ها"