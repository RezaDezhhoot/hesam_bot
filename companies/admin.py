from django.contrib import admin
from companies.models import Companieslink


@admin.register(Companieslink)
class CompaniesAdmin(admin.ModelAdmin):
    list_display = (
        'badge',
        'link',
    )
