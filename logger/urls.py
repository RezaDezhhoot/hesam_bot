﻿from django.urls import path
from logger.views import logs_d, logs_p, production_logs_data, debug_logs_data

urlpatterns = [
    path('d/', logs_d),
    path('p/', logs_p),
    path('d/data/', debug_logs_data),
    path('p/data/', production_logs_data),
]

