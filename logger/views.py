from datetime import datetime, time
import pytz
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.http import require_http_methods
from logger.models import LoggerDebug, LoggerProduction


def logger(respond_data, stage):
    # ---------------------- logs limit handle ---------------------
    number_of_d_object = LoggerDebug.objects.count()
    deviation = number_of_d_object - 50000
    if deviation > 0:
        LoggerDebug.objects.filter(
            id__in=list(LoggerDebug.objects.values_list('pk', flat=True)[:(deviation + 1000)])).delete()
    number_of_p_object = LoggerProduction.objects.count()
    deviation = number_of_p_object - 5000
    if deviation > 0:
        LoggerProduction.objects.filter(
            id__in=list(LoggerProduction.objects.values_list('pk', flat=True)[:(deviation + 100)])).delete()

    th_IR = pytz.timezone('Asia/Tehran')
    datetime_IR = datetime.now(th_IR)
    current_time = datetime_IR.strftime("%H:%M:%S.%f")[:-3]
    if stage == 'p':  # production level
        l_o_g = str(current_time) + " - " + str(respond_data)
        print(l_o_g)
        new_log = LoggerProduction(
            log=l_o_g,
        )
        new_log.save()
        new_log_debug = LoggerDebug(
            log=l_o_g,
        )
        new_log_debug.save()
    elif stage == 'd':  # debug level
        l_o_g = str(current_time) + " - " + str(respond_data)
        print(l_o_g)
        new_log = LoggerDebug(
            log=l_o_g,
        )
        new_log.save()


@never_cache
def logs_d(request):
    if request.user.is_authenticated and request.user.is_superuser:
        return render(request, 'logs_d.html', {'title': "logs_debug",
                                               'result': "",
                                               })
    else:
        return HttpResponse("you are not authorized to view the content")


@never_cache
def logs_p(request):
    if request.user.is_authenticated and request.user.is_superuser:
        return render(request, 'logs_p.html', {'title': "logs_production",
                                               'result': "",
                                               })
    else:
        return HttpResponse("you are not authorized to view the content")


@require_http_methods(["GET"])
@requires_csrf_token
@never_cache
def debug_logs_data(request):
    if request.method == "GET":
        log_list = []
        logs_ = LoggerDebug.objects.all().order_by('-id')[:100]
        for log in logs_:
            log_list.append(getattr(log, "log"))

        return JsonResponse({'result': log_list})


@require_http_methods(["GET"])
@requires_csrf_token
@never_cache
def production_logs_data(request):
    if request.method == "GET":
        log_list = []
        logs_ = LoggerProduction.objects.all().order_by('-id')[:50]
        for log in logs_:
            log_list.append(getattr(log, "log"))

        return JsonResponse({'result': log_list})
