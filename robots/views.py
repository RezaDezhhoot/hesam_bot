import ctypes
import threading
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.decorators.cache import never_cache
import robots.cbot
from logger.models import LoggerDebug, LoggerProduction
from robots.models import IsRobotRun
from robots.cbot import logger


@never_cache
def robot(request, ck):
    if request.method == 'GET':
        if request.user.is_authenticated and request.user.is_superuser:
            t1 = SecondThread(name="cbot")
            robot_check = IsRobotRun.objects.all()
            if robot_check.count() == 0:
                if ck == "start":
                    LoggerDebug.objects.all().delete()
                    LoggerProduction.objects.all().delete()
                    new_is_robot_run = IsRobotRun(
                        idx=1,
                        is_robot_run=True
                    )
                    new_is_robot_run.save()
                    t1.start()
                    return redirect('/logs/p')
                elif ck == "stop":
                    return HttpResponse("the robot is not running")
            else:
                robot_check_object = IsRobotRun.objects.get(idx=1)
                if getattr(robot_check_object, 'is_robot_run'):
                    if ck == "start":
                        return HttpResponse("the robot is running")
                    elif ck == "stop":
                        robot_check_object.is_robot_run = False
                        robot_check_object.save()
                        try:
                            t1.raise_exception()
                            t1.join()
                        except:
                            pass
                        return HttpResponse("the robot has stopped")
                elif not getattr(robot_check_object, 'is_robot_run'):
                    if ck == "start":
                        robot_check_object.is_robot_run = True
                        robot_check_object.save()
                        t1.start()
                        return redirect('/logs/p')
                    elif ck == "stop":
                        robot_check_object.is_robot_run = False
                        robot_check_object.save()
                        try:
                            t1.raise_exception()
                            t1.join()
                        except:
                            pass
                        return HttpResponse("the robot is not running")
        return HttpResponse("you are not authorized")
    return HttpResponse("you are not authorized")


class SecondThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        robots.cbot.bot()
        logger("robot execution started", 'd')

    def get_id(self):
        # returns id of the respective thread
        try:
            if hasattr(self, '_thread_id'):
                return self._thread_id
            for id, thread in threading._active.items():
                logger(thread, 'd')
                logger(id, 'd')
                logger("-----------", 'd')
                if str(thread).strip().find("cbot") != -1:
                    print("str(thread).strip().find(cbot): " + str(thread).strip())
                    return id
        except Exception as e:
            logger("exception in thread get id: " + str(e), 'd')

    def raise_exception(self):
        try:
            thread_id = self.get_id()
            res = ctypes.pythonapi.PyThreadState_SetAsyncExc(ctypes.c_long(thread_id),
                                                             ctypes.py_object(SystemExit))
            if res > 1:
                ctypes.pythonapi.PyThreadState_SetAsyncExc(ctypes.c_long(thread_id), 0)
                logger('Exception raise failure', 'd')
            logger("robot execution stopped", 'd')
        except Exception as e:
            logger("exception in thread raise exception: " + str(e), 'd')
