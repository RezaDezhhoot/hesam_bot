from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, WebDriverException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from datetime import datetime
import time
from codalbot.settings import DRIVER_PATH
from companies.models import Companieslink
from logger.views import logger
from proxies.models import get_proxy, check_proxy_availability
from robots.models import Company, MonthlyReport, SeasonalReport
from robots.utils import date_extractor, year_extractor, word_simplifire
from config_setting.models import ConfigSetting

from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

d = DesiredCapabilities.CHROME
d['goog:loggingPrefs'] = {'performance': 'ALL'}


def get_time_sleep():
    query_object = ConfigSetting.objects.all()
    if query_object.count() == 0:
        time_sleep = 30
    else:
        time_sleep = getattr(query_object[0], "sleep_time")
        logger("sleep time: " + str(time_sleep), "d")
    return int(time_sleep)


def get_codal_data(url, proxy_list):
    options = Options()
    options.headless = True
    options.add_argument("--window-size=1920,1200")
    settings = ConfigSetting.objects.all()[0]
    if settings.is_proxy_on:
        options.add_argument('--proxy-server=%s' % proxy_list)
    else:
        options.add_argument('--no-proxy-server')
    driver = webdriver.Chrome(options=options, executable_path=DRIVER_PATH, desired_capabilities=d)
    webdriver_problem_number_of_reloading = 0
    while True:
        try:
            number_of_reloading_page = 0
            while True:
                logger("get_codal_data: starting page load", "d")
                driver.get(url)
                # --------------- this happens only when chrome can't connect to internet---------------
                time.sleep(get_time_sleep())
                for entry in driver.get_log('performance'):
                    if str(entry['message']).find('"errorText":"net::ERR_TIMED_OUT"') != -1:
                        errorText = "net::ERR_NO_SUPPORTED_PROXIES"
                        logger(errorText, "d")
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_NO_SUPPORTED_PROXIES"') != -1:
                        errorText = "net::ERR_NO_SUPPORTED_PROXIES"
                        logger(errorText, "d")
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_INTERNET_DISCONNECTED"') != -1:
                        errorText = "net::ERR_INTERNET_DISCONNECTED"
                        logger(errorText, "d")
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_CONNECTION_TIMED_OUT"') != -1:
                        errorText = "net::ERR_CONNECTION_TIMED_OUT"
                        logger(errorText, "d")
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_CONNECTION_RESET"') != -1:
                        errorText = "net::ERR_CONNECTION_RESET"
                        logger(errorText, "d")
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_CONNECTION_REFUSED"') != -1:
                        errorText = "net::ERR_CONNECTION_REFUSED"
                        logger(errorText, "d")
                        raise ConnectionError
                logger("get_codal_data: spinner start", "d")
                try:
                    WebDriverWait(driver, 30).until(EC.invisibility_of_element_located((By.CLASS_NAME, "spinner")))
                    try:
                        retry_button = driver.find_element(By.XPATH,
                                                           "/html/body/form/div[3]/div[1]/div[1]/div[2]/div[2]/div/div/input")
                        logger("get_codal_data: loading of page has been failed. error: صفحه لود نشد", "d")
                        logger("waiting for: " + str(get_time_sleep()) + " seconds", "d")
                        time.sleep(get_time_sleep())
                    except:
                        break
                except Exception as e:
                    logger("get_codal_data: spinner problem, 30 second passed and spinner still spinning", "d")
                number_of_reloading_page += 1
                if number_of_reloading_page == 3:
                    driver.quit()
                    return logger(
                        "get_codal_data: after reloading had been tried 3 times, this page has not been received and passed",
                        "d")
            try:
                tbody = driver.find_element(By.XPATH,
                                            "/html/body/form/div[3]/div[1]/div[1]/div[2]/div[2]/table/tbody")
                tr = tbody.find_elements(By.TAG_NAME, "tr")
                for i in tr:
                    logger("______________________ start ____________________________", "d")
                    td = i.find_elements(By.CLASS_NAME, "table__content")

                    # td[0]>>>>>badge#
                    badge = td[0].text.strip()
                    badge_link = td[0].find_element(By.TAG_NAME, "a").get_attribute('href')

                    # td[1]>>>>>name#
                    name = td[1].text.strip()

                    # td[3]>>>>>description#
                    description = td[3].text.strip()
                    description_link = td[3].find_element(By.TAG_NAME, "a").get_attribute('href')
                    logger("badge: " + badge, "d")
                    logger("badge link: " + badge_link, "d")
                    logger("name: " + name, "d")
                    logger("description: " + description, "d")
                    logger("description link: " + description_link, "d")
                    try:
                        ye = year_extractor(description)
                        logger("year check: the year is: " + ye, "d")
                        """
                        similar code only the year is different and the year have to updates in future
                        """
                        try:
                            if ye == '1400' or ye == '1401' or ye == '1402' or ye == '1403' or ye == '1404':
                                try:
                                    old_company = Company.objects.get(badge=badge)
                                    old_company.updated_at = datetime.now()
                                    old_company.save()
                                    logger("this company exist" + " : " + badge, "d")
                                    if word_simplifire(description).find("گزارشفعالیتماهانهدوره۱ماههمنتهیبه") != -1:
                                        try:
                                            old_report = MonthlyReport.objects.get(name=description, company_name=badge)
                                            try:
                                                old_report.updated_at = datetime.now()
                                                old_report.link = description_link
                                                report_date = date_extractor(description)
                                                logger("date: " + str(report_date), "d")
                                                old_report.reported_date = report_date
                                                old_report.save()
                                                logger("this monthly report exist" + " : " + getattr(old_report,
                                                                                                     "company_name") + " - " + getattr(
                                                    old_report, "name") + " and updated", "d")
                                            except Exception as e:
                                                logger("try->except: old report data\n" + str(e), "d")
                                        except Exception as e:
                                            logger("try->except: old report\n" + str(e), "d")
                                            try:
                                                report_new = MonthlyReport(
                                                    company_name=badge,
                                                    name=description,
                                                    link=description_link,
                                                    reported_date=date_extractor(description),
                                                )
                                                report_new.save()
                                                old_company.monthly_report.add(report_new)
                                                old_company.save()
                                                logger(
                                                    "the new monthly report : " + badge + " - " + description + " has saved",
                                                    "d")
                                            except:
                                                logger("try->except: new report" + str(e), "d")
                                    elif word_simplifire(description).find("اطلاعاتوصورت‌هایمالیمیاندوره‌ای") != -1:
                                        try:
                                            old_report = SeasonalReport.objects.get(name=description,
                                                                                    company_name=badge)
                                            try:
                                                old_report.updated_at = datetime.now()
                                                old_report.link = description_link
                                                report_date = date_extractor(description)
                                                logger("date: " + str(report_date), "d")
                                                old_report.reported_date = report_date
                                                old_report.save()
                                                logger("this seasonal report exist" + " : " + getattr(old_report,
                                                                                                      "company_name") + " - " + getattr(
                                                    old_report, "name") + " and updated", "d")
                                            except Exception as e:
                                                logger("try->except: old report data\n" + str(e), "d")
                                        except Exception as e:
                                            logger("try->except: old report\n" + str(e), "d")
                                            try:
                                                report_new = SeasonalReport(
                                                    company_name=badge,
                                                    name=description,
                                                    link=description_link,
                                                    reported_date=date_extractor(description),
                                                )
                                                report_new.save()
                                                old_company.seasonal_report.add(report_new)
                                                old_company.save()
                                                logger(
                                                    "the new seasonal report : " + badge + " - " + description + " has saved",
                                                    "d")
                                            except:
                                                logger("try->except: new report" + str(e), "d")
                                    elif word_simplifire(description).find("صورت‌هایمالیسالمالیمنتهیبه") != -1:
                                        try:
                                            old_report = SeasonalReport.objects.get(name=description,
                                                                                    company_name=badge)
                                            try:
                                                old_report.updated_at = datetime.now()
                                                old_report.link = description_link
                                                report_date = date_extractor(description)
                                                logger("date: " + str(report_date), "d")
                                                old_report.reported_date = report_date
                                                old_report.save()
                                                logger("this seasonal report exist" + " : " + getattr(old_report,
                                                                                                      "company_name") + " - " + getattr(
                                                    old_report, "name") + " and updated", "d")
                                            except Exception as e:
                                                logger("try->except: old report data\n" + str(e), "d")
                                        except Exception as e:
                                            logger("try->except: old report\n" + str(e), "d")
                                            try:
                                                report_new = SeasonalReport(
                                                    company_name=badge,
                                                    name=description,
                                                    link=description_link,
                                                    reported_date=date_extractor(description),
                                                )
                                                report_new.save()
                                                old_company.seasonal_report.add(report_new)
                                                old_company.save()
                                                logger(
                                                    "the new seasonal report : " + badge + " - " + description + " has saved",
                                                    "d")
                                            except:
                                                logger("try->except: new report" + str(e), "d")
                                    elif word_simplifire(description).find("صورت‌هایمالیتلفیقیسالمالیمنتهیبه") != -1:
                                        try:
                                            old_report = SeasonalReport.objects.get(name=description,
                                                                                    company_name=badge)
                                            try:
                                                old_report.updated_at = datetime.now()
                                                old_report.link = description_link
                                                report_date = date_extractor(description)
                                                logger("date: " + str(report_date), "d")
                                                old_report.reported_date = report_date
                                                old_report.save()
                                                logger("this seasonal report exist" + " : " + getattr(old_report,
                                                                                                      "company_name") + " - " + getattr(
                                                    old_report, "name") + " and updated", "d")
                                            except Exception as e:
                                                logger("try->except: old report data\n" + str(e), "d")
                                        except Exception as e:
                                            logger("try->except: old report\n" + str(e), "d")
                                            try:
                                                report_new = SeasonalReport(
                                                    company_name=badge,
                                                    name=description,
                                                    link=description_link,
                                                    reported_date=date_extractor(description),
                                                )
                                                report_new.save()
                                                old_company.seasonal_report.add(report_new)
                                                old_company.save()
                                                logger(
                                                    "the new seasonal report : " + badge + " - " + description + " has saved",
                                                    "d")
                                            except:
                                                logger("try->except: new report" + str(e), "d")
                                    else:
                                        logger("گزارش این بخش با فرمت های تعریف شده سازگار نیست", 'd')
                                except Exception as e:
                                    logger("try->except: old company\n" + str(e), "d")
                                    try:
                                        company_new = Company(
                                            badge=badge,
                                            badge_link=badge_link,
                                            name=name,
                                        )
                                        company_new.save()
                                        logger("the new company : " + badge + " has saved", "d")
                                        if word_simplifire(description).find("گزارشفعالیتماهانهدوره۱ماههمنتهیبه") != -1:
                                            try:
                                                old_report = MonthlyReport.objects.get(name=description,
                                                                                       company_name=badge)
                                                try:
                                                    old_report.updated_at = datetime.now()
                                                    old_report.link = description_link
                                                    report_date = date_extractor(description)
                                                    logger("date: " + str(report_date), "d")
                                                    old_report.reported_date = report_date
                                                    old_report.save()
                                                    logger("this monthly report exist" + " : " + getattr(old_report,
                                                                                                         "company_name") + " - " + getattr(
                                                        old_report, "name") + " and updated", "d")
                                                except Exception as e:
                                                    logger("try->except: old report data" + str(e), "d")
                                            except Exception as e:
                                                logger("try->except: old report" + str(e), "d")
                                                try:
                                                    report_new = MonthlyReport(
                                                        company_name=badge,
                                                        name=description,
                                                        link=description_link,
                                                        reported_date=date_extractor(description),
                                                    )
                                                    report_new.save()
                                                    company_new.monthly_report.add(report_new)
                                                    company_new.save()
                                                    logger(
                                                        "the new monthly report : " + badge + " - " + description + " has saved",
                                                        "d")
                                                except Exception as e:
                                                    logger("try->except: new report" + str(e), "d")
                                        elif word_simplifire(description).find("اطلاعاتوصورت‌هایمالیمیاندوره‌ای") != -1:
                                            try:
                                                old_report = SeasonalReport.objects.get(name=description,
                                                                                        company_name=badge)
                                                try:
                                                    old_report.updated_at = datetime.now()
                                                    old_report.link = description_link
                                                    report_date = date_extractor(description)
                                                    logger("date: " + str(report_date), "d")
                                                    old_report.reported_date = report_date
                                                    old_report.save()
                                                    logger("this seasonal report exist" + " : " + getattr(old_report,
                                                                                                          "company_name") + " - " + getattr(
                                                        old_report, "name") + " and updated", "d")
                                                except Exception as e:
                                                    logger("try->except: old report data" + str(e), "d")
                                            except Exception as e:
                                                logger("try->except: old report" + str(e), "d")
                                                try:
                                                    report_new = SeasonalReport(
                                                        company_name=badge,
                                                        name=description,
                                                        link=description_link,
                                                        reported_date=date_extractor(description),
                                                    )
                                                    report_new.save()
                                                    company_new.seasonal_report.add(report_new)
                                                    company_new.save()
                                                    logger(
                                                        "the new seasonal report : " + badge + " - " + description + " has saved",
                                                        "d")
                                                except Exception as e:
                                                    logger("try->except: new report " + str(e), "d")
                                        elif word_simplifire(description).find("صورت‌هایمالیسالمالیمنتهیبه") != -1:
                                            try:
                                                old_report = SeasonalReport.objects.get(name=description,
                                                                                        company_name=badge)
                                                try:
                                                    old_report.updated_at = datetime.now()
                                                    old_report.link = description_link
                                                    report_date = date_extractor(description)
                                                    logger("date: " + str(report_date), "d")
                                                    old_report.reported_date = report_date
                                                    old_report.save()
                                                    logger("this seasonal report exist" + " : " + getattr(old_report,
                                                                                                          "company_name") + " - " + getattr(
                                                        old_report, "name") + " and updated", "d")
                                                except Exception as e:
                                                    logger("try->except: old report data" + str(e), "d")
                                            except Exception as e:
                                                logger("try->except: old report" + str(e), "d")
                                                try:
                                                    report_new = SeasonalReport(
                                                        company_name=badge,
                                                        name=description,
                                                        link=description_link,
                                                        reported_date=date_extractor(description),
                                                    )
                                                    report_new.save()
                                                    company_new.seasonal_report.add(report_new)
                                                    company_new.save()
                                                    logger(
                                                        "the new seasonal report : " + badge + " - " + description + " has saved",
                                                        "d")
                                                except Exception as e:
                                                    logger("try->except: new report " + str(e), "d")
                                        elif word_simplifire(description).find("صورت‌هایمالیتلفیقیسالمالیمنتهیبه") != -1:
                                            try:
                                                old_report = SeasonalReport.objects.get(name=description,
                                                                                        company_name=badge)
                                                try:
                                                    old_report.updated_at = datetime.now()
                                                    old_report.link = description_link
                                                    report_date = date_extractor(description)
                                                    logger("date: " + str(report_date), "d")
                                                    old_report.reported_date = report_date
                                                    old_report.save()
                                                    logger("this seasonal report exist" + " : " + getattr(old_report,
                                                                                                          "company_name") + " - " + getattr(
                                                        old_report, "name") + " and updated", "d")
                                                except Exception as e:
                                                    logger("try->except: old report data " + str(e), "d")
                                            except Exception as e:
                                                logger("try->except: old report " + str(e), "d")
                                                try:
                                                    report_new = SeasonalReport(
                                                        company_name=badge,
                                                        name=description,
                                                        link=description_link,
                                                        reported_date=date_extractor(description),
                                                    )
                                                    report_new.save()
                                                    company_new.seasonal_report.add(report_new)
                                                    company_new.save()
                                                    logger(
                                                        "the new seasonal report : " + badge + " - " + description + " has saved",
                                                        "d")
                                                except Exception as e:
                                                    logger("try->except: new report " + str(e), "d")
                                        else:
                                            logger("گزارش این بخش با فرمت های تعریف شده سازگار نیست", 'd')
                                    except Exception as e:
                                        logger("try->except: new company cant create \n" + str(e), "d")

                            else:
                                logger("the year in description is not 1400-1403", "d")
                        except Exception as e:
                            logger("unknown exception: " + str(e), 'd')
                    except Exception as e:
                        logger(str(e), 'd')
                        logger("the year in description is not in range", "d")
                    logger("______________________ end ____________________________", "d")
            except Exception as e:
                logger("try->except: get_codal_data tbody" + str(e), "d")
            break
        except NoSuchElementException as noelement:
            logger('get_codal_data no such element exception: ' + str(noelement), "d")
            logger('we are waiting for ' + str(get_time_sleep()) + ' second', "d")
            time.sleep(get_time_sleep())
        except WebDriverException as driver_exception:
            logger('get_codal_data webdriver exception: ' + str(driver_exception), "d")
            logger('we are waiting for ' + str(get_time_sleep()) + ' second', "d")
            time.sleep(get_time_sleep())
        except ConnectionError as e:
            raise e
        except Exception as e:
            logger('get_codal_data other exception: ' + str(e), "d")
            logger('we are waiting for ' + str(get_time_sleep()) + ' second', "d")
            time.sleep(get_time_sleep())
        webdriver_problem_number_of_reloading += 1
        if webdriver_problem_number_of_reloading == 3:
            driver.quit()
            return logger(
                "get_codal_data: after reloading had been tried 3 times, this webdriver exceptions still exist", 'd')
    logger('get_codal_data:' + str(url) + ' | complete', "d")
    driver.quit()


def get_monthly_report_number(report_object, report_link, proxy_list):
    options = Options()
    options.headless = True
    settings = ConfigSetting.objects.all()[0]
    if settings.is_proxy_on:
        options.add_argument('--proxy-server=%s' % proxy_list)
    else:
        options.add_argument('--no-proxy-server')
    options.add_argument("--window-size=1920,1200")
    driver = webdriver.Chrome(options=options, executable_path=DRIVER_PATH, desired_capabilities=d)
    webdriver_problem_number_of_reloading = 0
    while True:
        try:
            number_of_reloading_page = 0
            while True:
                logger("get_monthly_report_number: starting page load", "d")
                url = report_link
                driver.get(url)
                # --------------- this happens only when chrome can't connect to internet---------------
                time.sleep(get_time_sleep())
                for entry in driver.get_log('performance'):
                    if str(entry['message']).find('"errorText":"net::ERR_TIMED_OUT"') != -1:
                        errorText = "net::ERR_NO_SUPPORTED_PROXIES"
                        logger(errorText, "d")
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_NO_SUPPORTED_PROXIES"') != -1:
                        errorText = "net::ERR_NO_SUPPORTED_PROXIES"
                        logger(errorText, "d")
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_INTERNET_DISCONNECTED"') != -1:
                        errorText = "net::ERR_INTERNET_DISCONNECTED"
                        logger(errorText, "d")
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_CONNECTION_TIMED_OUT"') != -1:
                        errorText = "net::ERR_CONNECTION_TIMED_OUT"
                        logger(errorText, "d")
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_CONNECTION_RESET"') != -1:
                        errorText = "net::ERR_CONNECTION_RESET"
                        logger(errorText, "d")
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_CONNECTION_REFUSED"') != -1:
                        errorText = "net::ERR_CONNECTION_REFUSED"
                        logger(errorText, "d")
                        raise ConnectionError
                try:
                    WebDriverWait(driver, 30).until(EC.invisibility_of_element_located((By.CLASS_NAME, "spinner")))
                    WebDriverWait(driver, 30).until(EC.visibility_of_element_located((By.CLASS_NAME, "label")))
                    try:
                        retry_button = driver.find_element(By.XPATH,
                                                           "/html/body/form/div[3]/div[1]/div[1]/div[2]/div[2]/div/div/input")
                        logger("get_monthly_report_number: loading of page has been failed. error: صفحه لود نشد", "d")
                        logger("waiting for: " + "60" + "seconds", "d")
                        time.sleep(get_time_sleep())
                    except Exception as e:
                        logger("try->except get_monthly_report_number retry_button - " + str(e), 'd')
                        break
                except Exception as e:
                    logger(
                        "get_monthly_report_number: page timeout problem, 30 second passed and nothing happened - " + str(
                            e), 'd')
                number_of_reloading_page += 1
                if number_of_reloading_page == 3:
                    driver.quit()
                    return logger(
                        "get_monthly_report_number: after reloading had been tried 3 times, this page has not been received and passed",
                        'd')
            page_is_ok = False
            try:
                option_ = driver.find_elements(By.TAG_NAME, 'option')
                for op in option_:
                    op_text = word_simplifire(op.text.strip())
                    if op_text == "گزارشفعالیتماهانه":
                        page_is_ok = True
                        print(5)
                        logger("get_monthly_report_number | page_is_ok? : " + "TRUE", 'd')
            except:
                print("get_monthly_report_number | page_is_ok? : " + "FALSE")
            if page_is_ok:
                while True:
                    try:
                        table_title = driver.find_element(By.CLASS_NAME, "table-title")
                        app_table = driver.find_elements(By.CLASS_NAME, "rayanDynamicStatement")[0]
                        tbody = app_table.find_element(By.TAG_NAME, 'tbody')
                        tr = tbody.find_elements(By.TAG_NAME, "tr")
                        last_tr = tr[len(tr) - 1]
                        td = last_tr.find_elements(By.TAG_NAME, "td")
                        table_title_text = word_simplifire(table_title.text)

                        if table_title_text == "تولیدوفروش":
                            try:
                                logger("report url: " + url, 'd')
                                logger("category is: " + "تولید و فروش", 'd')
                                logger("reported_number is: " + td[16].text, 'd')
                                number_txt = td[16].text
                                number_txt = number_txt.replace(",", "")
                                number = int(number_txt)
                                report_object.reported_number = number
                                report_object.is_finished = True
                                report_object.save()
                            except Exception as e:
                                logger("try->except monthly report - تولید و فروش - " + str(e), 'd')
                        elif table_title_text == "خدماتوفروش":
                            try:
                                logger("report url: " + url, 'd')
                                logger("category is: " + "خدمات و فروش", 'd')
                                logger("reported_number is: " + td[6].text, 'd')
                                number_txt = td[6].text
                                number_txt = number_txt.replace(",", "")
                                number = int(number_txt)
                                report_object.reported_number = number
                                report_object.is_finished = True
                                report_object.save()
                            except Exception as e:
                                logger("try->except monthly report - خدمات و فروش - " + str(e), 'd')
                        elif table_title_text == "فرمدرامدهایعملیاتیماهانه":
                            try:
                                logger("report url: " + url, 'd')
                                logger("category is: " + "فرم درآمدهای عملیاتی ماهانه", 'd')
                                logger("reported_number is: " + td[16].text, 'd')
                                number_txt = td[16].text
                                number_txt = number_txt.replace(",", "")
                                number = int(number_txt)
                                report_object.reported_number = number
                                report_object.is_finished = True
                                report_object.save()
                            except Exception as e:
                                logger("try->except monthly report - فرم درآمدهای عملیاتی ماهانه - " + str(e), 'd')
                        elif table_title_text == "محصولات":
                            try:
                                logger("report url: " + url, 'd')
                                logger("category is: " + "محصولات", 'd')
                                logger("reported_number is: " + td[16].text, 'd')
                                number_txt = td[16].text
                                number_txt = number_txt.replace(",", "")
                                number = int(number_txt)
                                report_object.reported_number = number
                                report_object.is_finished = True
                                report_object.save()
                            except Exception as e:
                                logger("try->except monthly report - محصولات - " + str(e), 'd')
                        else:
                            logger("report url: " + url, 'd')
                            logger("category is: " + "unknown category", 'd')
                            report_object.is_finished = True
                            report_object.is_acceptable = False
                            report_object.save()
                        driver.quit()
                        break
                    except Exception as e:
                        logger(str(e) + "\nthis page has no proper report, for more information check the url below: ",
                               'd')
                        logger("report url: " + url, 'd')
                        logger("category is: " + "unknown category", 'd')
                        report_object.is_finished = True
                        report_object.is_acceptable = False
                        report_object.save()
                        break
            else:
                logger("get_monthly_report_number page in not ok / unknown category", 'd')
                logger("this page has no proper report, for more information check the url below: ", 'd')
                logger("report url: " + url, 'd')
                logger("category is: " + "unknown category", 'd')
                report_object.is_finished = True
                report_object.is_acceptable = False
                report_object.save()
            break
        except NoSuchElementException as noelement:
            logger('get_monthly_report_number no such element exception: ' + str(noelement), 'd')
            logger('we are waiting for ' + str(get_time_sleep()) + ' second', 'd')
            time.sleep(get_time_sleep())
        except WebDriverException as driver_exception:
            logger('get_monthly_report_number webdriver exception: ' + str(driver_exception), 'd')
            logger('we are waiting for ' + str(get_time_sleep()) + ' second', 'd')
            time.sleep(get_time_sleep())
        except ConnectionError as e:
            raise
        except Exception as e:
            logger('get_monthly_report_number other exception: ' + str(e), 'd')
            logger('we are waiting for ' + str(get_time_sleep()) + ' second', 'd')
            time.sleep(get_time_sleep())
        webdriver_problem_number_of_reloading += 1
        if webdriver_problem_number_of_reloading == 3:
            driver.quit()
            return logger(
                "get_monthly_report_number: after reloading had been tried 3 times, this webdriver exceptions still exist",
                'd')
    logger('get_monthly_report_number:' + str(report_object) + ' | complete', 'd')
    driver.quit()


def get_seasonal_report_number(report_object, report_link, proxy_list):
    options = Options()
    options.headless = True
    settings = ConfigSetting.objects.all()[0]
    if settings.is_proxy_on:
        options.add_argument('--proxy-server=%s' % proxy_list)
    else:
        options.add_argument('--no-proxy-server')
    options.add_argument("--window-size=1920,1200")
    driver = webdriver.Chrome(options=options, executable_path=DRIVER_PATH, desired_capabilities=d)
    webdriver_problem_number_of_reloading = 0
    while True:
        try:
            number_of_reloading_page = 0
            page_is_ok = False
            while True:
                logger("get_seasonal_report_number: starting page load", 'd')
                url = report_link
                driver.get(url)
                # --------------- this happens only when chrome can't connect to internet---------------
                time.sleep(get_time_sleep())
                for entry in driver.get_log('performance'):
                    if str(entry['message']).find('"errorText":"net::ERR_TIMED_OUT"') != -1:
                        errorText = "net::ERR_NO_SUPPORTED_PROXIES"
                        logger(errorText, "d")
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_NO_SUPPORTED_PROXIES"') != -1:
                        errorText = "net::ERR_NO_SUPPORTED_PROXIES"
                        logger(errorText, 'd')
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_INTERNET_DISCONNECTED"') != -1:
                        errorText = "net::ERR_INTERNET_DISCONNECTED"
                        logger(errorText, 'd')
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_CONNECTION_TIMED_OUT"') != -1:
                        errorText = "net::ERR_CONNECTION_TIMED_OUT"
                        logger(errorText, 'd')
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_CONNECTION_RESET"') != -1:
                        errorText = "net::ERR_CONNECTION_RESET"
                        logger(errorText, 'd')
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_CONNECTION_REFUSED"') != -1:
                        errorText = "net::ERR_CONNECTION_REFUSED"
                        logger(errorText, 'd')
                        raise ConnectionError
                try:
                    WebDriverWait(driver, 30).until(EC.visibility_of_element_located((By.CLASS_NAME, "label")))
                    option_ = driver.find_elements(By.TAG_NAME, 'option')
                    for op in option_:
                        op_text = word_simplifire(op.text.strip())
                        if op_text == "صورتسودوزیان":
                            op.click()
                            break
                    WebDriverWait(driver, 30).until(
                        EC.visibility_of_element_located((By.CLASS_NAME, "table-containet")))
                    page_is_ok = True
                    logger("get_seasonal_report_number | page_is_ok? : " + "TRUE", 'd')
                    break
                except Exception as e:
                    logger("try->except get_seasonal_report_number  label doesnt load\n" + str(e), 'd')
                    logger("get_seasonal_report_number: page loading problem, 30 second passed and nothing happened",
                           'd')
                number_of_reloading_page += 1
                if number_of_reloading_page == 3:
                    driver.quit()
                    return logger(
                        "get_seasonal_report_number: after reloading had been tried 3 times, this page has not been received and passed",
                        'd')
            if page_is_ok:
                try:
                    logger("get_seasonal_report_number url : \n" + str(url), 'd')
                    try:
                        pass_if_exist = driver.find_element(By.CLASS_NAME, "table_holder")
                        logger(pass_if_exist.text.strip(), 'd')
                    except Exception as e:
                        logger("try->except get_seasonal_report_number table_holder not exist\n" + str(e), 'd')
                        tds = driver.find_elements(By.TAG_NAME, "td")
                        n = 0
                        for td in tds:
                            spans = td.find_elements(By.TAG_NAME, "span")
                            m = 0
                            for span in spans:
                                span_text = word_simplifire(span.text)
                                if span_text.find("درامدحقبیمهناخالص") != -1:
                                    logger("get_seasonal_report_number is درآمد حق بیمه ناخالص", 'd')
                                    m = 1
                                    break
                                else:
                                    if span_text.find("سودزیانپایههرسهم") != -1:
                                        logger(span.text, 'd')
                                        logger(n, 'd')
                                        m = 1
                                        if tds[n - 5].text != "":
                                            first_td = tds[n - 5]
                                            first_td = first_td.text
                                            first_td = number_handler(first_td)
                                            logger(first_td, 'd')
                                            if tds[n - 4].text != "":
                                                second_td = tds[n - 4]
                                                second_td = second_td.text
                                                second_td = number_handler(second_td)
                                                logger(second_td, 'd')
                                            elif tds[n - 4].text == "" and tds[n - 3].text != "":
                                                second_td = tds[n - 3]
                                                second_td = second_td.text
                                                second_td = number_handler(second_td)
                                                logger(second_td, 'd')
                                            elif tds[n - 4].text == "" and tds[n - 3].text == "" and tds[
                                                n - 2].text != "":
                                                second_td = tds[n - 2]
                                                second_td = second_td.text
                                                second_td = number_handler(second_td)
                                                logger(second_td, 'd')
                                        elif tds[n - 5].text == "" and tds[n - 4].text != "":
                                            first_td = tds[n - 4]
                                            first_td = first_td.text
                                            first_td = number_handler(first_td)
                                            logger(first_td, 'd')
                                            if tds[n - 3].text != "":
                                                second_td = tds[n - 3]
                                                second_td = second_td.text
                                                second_td = number_handler(second_td)
                                                logger(second_td, 'd')
                                            elif tds[n - 3].text == "" and tds[n - 2].text != "":
                                                second_td = tds[n - 2]
                                                second_td = second_td.text
                                                second_td = number_handler(second_td)
                                                logger(second_td, 'd')
                                        elif tds[n - 5].text == "" and tds[n - 4].text == "" and tds[n - 3].text != "":
                                            first_td = tds[n - 3]
                                            first_td = first_td.text
                                            first_td = number_handler(first_td)
                                            logger(first_td, 'd')
                                            second_td = tds[n - 2]
                                            second_td = second_td.text
                                            second_td = number_handler(second_td)
                                            logger(second_td, 'd')
                                        break
                                    else:
                                        pass
                            if m == 1:
                                break
                            n += 1
                    try:
                        if float(first_td) > 0 and float(second_td) > 0:
                            if abs(float(first_td)) > abs(float(second_td)):
                                final_number_percentage = (abs(float(first_td) / float(second_td)) - 1) * 100
                                final_number_percentage = round(final_number_percentage)
                                report_object.reported_percentage = final_number_percentage
                                report_object.reported_percentage_color = 'black'
                                report_object.is_finished = True
                                report_object.save()
                                logger(str(final_number_percentage), 'd')
                            else:
                                final_number_percentage = (abs(float(first_td) / float(second_td)) - 1) * 100
                                final_number_percentage = round(final_number_percentage)
                                report_object.reported_percentage = final_number_percentage
                                report_object.reported_percentage_color = 'black'
                                report_object.is_finished = True
                                report_object.save()
                                logger(str(final_number_percentage), 'd')
                        elif float(first_td) < 0 and float(second_td) < 0:
                            if abs(float(first_td)) > abs(float(second_td)):
                                final_number_percentage = (abs(float(first_td) / float(second_td)) - 1) * 100
                                final_number_percentage = round(final_number_percentage)
                                report_object.reported_percentage = final_number_percentage * (-1)
                                report_object.reported_percentage_color = 'red'
                                report_object.is_finished = True
                                report_object.save()
                                logger(str(final_number_percentage), 'd')
                            else:
                                final_number_percentage = (abs(float(first_td) / float(second_td)) - 1) * 100
                                final_number_percentage = round(final_number_percentage) * -1
                                report_object.reported_percentage = final_number_percentage
                                report_object.reported_percentage_color = 'blue'
                                report_object.is_finished = True
                                report_object.save()
                                logger(str(final_number_percentage), 'd')
                        elif float(first_td) > 0 > float(second_td):
                            final_number_percentage = ((abs(float(first_td) / float(second_td))) + 1) * 100
                            final_number_percentage = round(final_number_percentage)
                            report_object.reported_percentage = final_number_percentage
                            report_object.reported_percentage_color = 'green'
                            report_object.is_finished = True
                            report_object.save()
                            logger(str(final_number_percentage), 'd')
                        elif float(first_td) < 0 < float(second_td):
                            final_number_percentage = (abs((float(first_td)) / float(second_td)) + 1) * 100
                            final_number_percentage = round(final_number_percentage)
                            report_object.reported_percentage = final_number_percentage
                            report_object.reported_percentage_color = 'red'
                            report_object.is_finished = True
                            report_object.save()
                            logger(str(final_number_percentage), 'd')
                    except ValueError:
                        logger("try->except get_seasonal_report_number value error", 'd')
                        final_number_percentage = "not available"
                        logger(str(final_number_percentage), 'd')
                        report_object.is_finished = True
                        report_object.is_acceptable = False
                        report_object.save()
                    break
                except Exception as e:
                    logger("message: " + str(e), 'd')
                    logger("this company has no proper report, for more information chek the url below: ", 'd')
                    report_object.is_finished = True
                    report_object.is_acceptable = False
                    report_object.save()
            else:
                logger("get_seasonal_report_number: page is not ok", 'd')
                logger("this company has no proper report, for more information chek the url below: ", 'd')
                report_object.is_finished = True
                report_object.is_acceptable = False
                report_object.save()
            break
        except NoSuchElementException as noelement:
            logger('get_seasonal_report_number no such element exception: ' + str(noelement), 'd')
            logger('we are waiting for ' + str(get_time_sleep()) + ' second', 'd')
            time.sleep(get_time_sleep())
        except WebDriverException as driver_exception:
            logger('get_seasonal_report_number webdriver exception: ' + str(driver_exception), 'd')
            logger('we are waiting for ' + str(get_time_sleep()) + ' second', 'd')
            time.sleep(get_time_sleep())
        except ConnectionError as e:
            raise
        except Exception as e:
            logger('get_seasonal_report_number other exception: ' + str(e), 'd')
            logger('we are waiting for ' + str(get_time_sleep()) + ' second', 'd')
            time.sleep(get_time_sleep())
        webdriver_problem_number_of_reloading += 1
        if webdriver_problem_number_of_reloading == 3:
            driver.quit()
            return logger(
                "get_seasonal_report_number: after reloading had been tried 3 times, this webdriver exceptions still exist",
                'd')
    logger('get_seasonal_report_number:' + str(report_object) + ' | complete', 'd')
    driver.quit()


def check_if_last_page(url, proxy_list):
    options = Options()
    options.headless = True
    settings = ConfigSetting.objects.all()[0]
    if settings.is_proxy_on:
        options.add_argument('--proxy-server=%s' % proxy_list)
    else:
        options.add_argument('--no-proxy-server')
    options.add_argument("--window-size=1920,1200")
    driver = webdriver.Chrome(options=options, executable_path=DRIVER_PATH, desired_capabilities=d)
    webdriver_problem_number_of_reloading = 0
    while True:
        try:
            number_of_reloading_page = 0
            while True:
                logger("start check_if_last_page", 'd')
                driver.get(url)
                # --------------- this happens only when chrome can't connect to internet---------------
                time.sleep(get_time_sleep())
                for entry in driver.get_log('performance'):
                    if str(entry['message']).find('"errorText":"net::ERR_TIMED_OUT"') != -1:
                        errorText = "net::ERR_NO_SUPPORTED_PROXIES"
                        logger(errorText, "d")
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_NO_SUPPORTED_PROXIES"') != -1:
                        errorText = "net::ERR_NO_SUPPORTED_PROXIES"
                        logger(errorText, 'd')
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_INTERNET_DISCONNECTED"') != -1:
                        errorText = "net::ERR_INTERNET_DISCONNECTED"
                        logger(errorText, 'd')
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_CONNECTION_TIMED_OUT"') != -1:
                        errorText = "net::ERR_CONNECTION_TIMED_OUT"
                        logger(errorText, 'd')
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_CONNECTION_RESET"') != -1:
                        errorText = "net::ERR_CONNECTION_RESET"
                        logger(errorText, 'd')
                        raise ConnectionError
                    elif str(entry['message']).find('"errorText":"net::ERR_CONNECTION_REFUSED"') != -1:
                        errorText = "net::ERR_CONNECTION_REFUSED"
                        logger(errorText, 'd')
                        raise ConnectionError
                logger("spinner start", 'd')
                try:
                    WebDriverWait(driver, 30).until(EC.invisibility_of_element_located((By.CLASS_NAME, "spinner")))
                    try:
                        retry_button = driver.find_element(By.XPATH,
                                                           "/html/body/form/div[3]/div[1]/div[1]/div[2]/div[2]/div/div/input")
                        logger("loading of page has been failed. error: صفحه لود نشد", 'd')
                        time_sleep = 60
                        logger("waiting for: " + str(time_sleep) + "seconds", 'd')
                        time.sleep(time_sleep)
                    except Exception as e:
                        logger("page has loaded", 'd')
                        try:
                            app_table = driver.find_element(By.CLASS_NAME, "text-danger")
                            logger(app_table.text.strip(), 'd')
                            driver.quit()
                            logger("this is last page", 'd')
                            return True
                        except Exception as e:
                            driver.quit()
                            logger("this is not last page", 'd')
                            return False
                except Exception as e:
                    logger("check_if_last_page: spinner problem, 30 second passed and spinner still spinning", 'd')
                number_of_reloading_page += 1
                if number_of_reloading_page == 3:
                    driver.quit()
                    logger(
                        "check_if_last_page: after reloading had been tried 3 times, this page has not been received and passed",
                        'd')
                    return True
        except NoSuchElementException as noelement:
            logger('check_if_last_page no such element exception: ' + str(noelement), 'd')
            logger('we are waiting for ' + str(get_time_sleep()) + ' second', 'd')
            time.sleep(get_time_sleep())
        except WebDriverException as driver_exception:
            logger('check_if_last_page webdriver exception: ' + str(driver_exception), 'd')
            logger('we are waiting for ' + str(get_time_sleep()) + ' second', 'd')
            time.sleep(get_time_sleep())
        except ConnectionError as e:
            raise
        except Exception as e:
            logger('check_if_last_page other exception: ' + str(e), 'd')
            logger('we are waiting for ' + str(get_time_sleep()) + ' second', 'd')
            time.sleep(get_time_sleep())
        webdriver_problem_number_of_reloading += 1
        if webdriver_problem_number_of_reloading == 3:
            driver.quit()
            logger("check_if_last_page: after reloading had been tried 3 times, this webdriver exceptions still exist",
                   'd')
            return True


def codal_first_page():
    url = "https://www.codal.ir/ReportList.aspx?search&LetterType=-1&FromDate=1400%2F01%2F01&AuditorRef=-1&PageNumber=1&Audited&NotAudited&IsNotAudited=false&Childs=false&Mains&Publisher=false&CompanyState=-1&Category=-1&CompanyType=1&Consolidatable&NotConsolidatable"
    return url


def codal_monthly_report_page(company_link, n):
    url = str(company_link) + "&LetterType=-1&FromDate=1400%2F01%2F01&AuditorRef=-1&PageNumber=" + str(
        n) + "&Audited&NotAudited&IsNotAudited=false&Childs=false&Mains&Publisher=false&CompanyState=-1&Category=3&CompanyType=-1&Consolidatable&NotConsolidatable"
    return url


def codal_seasonal_report_page(company_link, n):
    url = str(company_link) + "&LetterType=6&FromDate=1400%2F01%2F01&AuditorRef=-1&PageNumber=" + str(
        n) + "&Audited&NotAudited&IsNotAudited=false&Childs=false&Mains&Publisher=false&CompanyState=-1&Category=1&CompanyType=-1&Consolidatable&NotConsolidatable"
    return url


def number_handler(string):
    string = string.replace(",", "")
    if string.find("(") != -1:
        string = string.replace("(", "-")
        string = string.replace(")", "")
    return string


def bot():
    while True:
        logger("the robot has started task 1", 'p')
        while True:
            try:
                logger("check and update data with first page", 'p')
                get_codal_data(codal_first_page(), get_proxy()['https'])
                logger(str(get_time_sleep()) + "second delay after updating data with first page", 'p')
                time.sleep(get_time_sleep())
                break
            except Exception as e:
                logger("problem has happened during the event : " + str(e), 'p')
                while True:
                    logger("checking proxy...", 'p')
                    if check_proxy_availability(get_proxy())[0]:
                        break
                    else:
                        logger("proxies are not connectable. pls upgrade theme", 'p')
                        logger("we check proxy list again after 5 minute", 'p')
                        time.sleep(300)
        companies_link = Companieslink.objects.all()
        while companies_link.count() != 0:
            qq = companies_link.latest('id')
            company_badge = getattr(qq, "badge")
            company_link = getattr(qq, "link")
            logger("cbot - نماد کمپانی : \n" + str(company_badge), 'p')
            logger("cbot - لینک کمپانی : \n" + str(company_link), 'p')
            n = 1
            while True:
                try:
                    while not check_if_last_page(codal_monthly_report_page(company_link, n), get_proxy()['https']):
                        logger(str(get_time_sleep()) + "second delay after checking " + company_badge + " monthly last page", 'p')
                        time.sleep(get_time_sleep())
                        while True:
                            try:
                                logger("check and update data with first page", 'p')
                                get_codal_data(codal_first_page(), get_proxy()['https'])
                                logger(str(get_time_sleep()) + "second delay after updating data with first page", 'p')
                                time.sleep(get_time_sleep())
                                break
                            except Company.DoesNotExist as e:
                                logger("company problem : " + str(e), 'p')
                                break
                            except Exception as e:
                                logger("problem has happened during the event : " + str(e), 'p')
                                while True:
                                    logger("checking proxy...", 'p')
                                    if check_proxy_availability(get_proxy())[0]:
                                        break
                                    else:
                                        logger("proxies are not connectable. pls upgrade theme", 'p')
                                        logger("we check proxy list again after 5 minute", 'p')
                                        time.sleep(300)
                        while True:
                            try:
                                logger("check and update " + company_badge + " monthly report", 'p')
                                get_codal_data(codal_monthly_report_page(company_link, n), get_proxy()['https'])
                                logger(str(get_time_sleep()) + "second delay after getting monthly report", 'p')
                                time.sleep(get_time_sleep())
                                break
                            except Company.DoesNotExist as e:
                                logger("company problem : " + str(e), 'p')
                                break
                            except Exception as e:
                                logger("problem has happened during the event : " + str(e), 'p')
                                while True:
                                    logger("checking proxy...", 'p')
                                    if check_proxy_availability(get_proxy())[0]:
                                        break
                                    else:
                                        logger("proxies are not connectable. pls upgrade theme", 'p')
                                        logger("we check proxy list again after 5 minute", 'p')
                                        time.sleep(300)
                        n += 1
                    break
                except Company.DoesNotExist as e:
                    logger("company problem : " + str(e), 'p')
                    n += 1
                except Exception as e:
                    logger("problem has happened during the event : " + str(e), 'p')
                    while True:
                        logger("checking proxy...", 'p')
                        if check_proxy_availability(get_proxy())[0]:
                            break
                        else:
                            logger("proxies are not connectable. pls upgrade theme", 'p')
                            logger("we check proxy list again after 5 minute", 'p')
                            time.sleep(300)
            n = 1
            while True:
                try:
                    while not check_if_last_page(codal_seasonal_report_page(company_link, n), get_proxy()['https']):
                        logger(str(get_time_sleep()) + "second delay after checking " + company_badge + " seasonal last page", 'p')
                        time.sleep(get_time_sleep())
                        while True:
                            try:
                                logger("check and update data with first page", 'p')
                                get_codal_data(codal_first_page(), get_proxy()['https'])
                                logger(str(get_time_sleep()) + "second delay after updating data with first page", 'p')
                                time.sleep(get_time_sleep())
                                break
                            except Company.DoesNotExist as e:
                                logger("company problem : " + str(e), 'p')
                                break
                            except Exception as e:
                                logger("problem has happened during the event : " + str(e), 'p')
                                while True:
                                    logger("checking proxy...", 'p')
                                    if check_proxy_availability(get_proxy())[0]:
                                        break
                                    else:
                                        logger("proxies are not connectable. pls upgrade theme", 'p')
                                        logger("we check proxy list again after 5 minute", 'p')
                                        time.sleep(300)
                        while True:
                            try:
                                logger("check and update " + company_badge + " seasonal report", 'p')
                                get_codal_data(codal_seasonal_report_page(company_link, n), get_proxy()['https'])
                                logger(str(get_time_sleep()) + "second delay after getting seasonal report", 'p')
                                time.sleep(get_time_sleep())
                                break
                            except Company.DoesNotExist as e:
                                logger("company problem : " + str(e), 'p')
                                break
                            except Exception as e:
                                logger("problem has happened during the event : " + str(e), 'p')
                                while True:
                                    logger("checking proxy...", 'p')
                                    if check_proxy_availability(get_proxy())[0]:
                                        break
                                    else:
                                        logger("proxies are not connectable. pls upgrade theme", 'p')
                                        logger("we check proxy list again after 5 minute", 'p')
                                        time.sleep(300)
                        n += 1
                    break
                except Company.DoesNotExist as e:
                    logger("company problem : " + str(e), 'p')
                    n += 1
                except Exception as e:
                    logger("problem has happened during the event : " + str(e), 'p')
                    while True:
                        logger("checking proxy...", 'p')
                        if check_proxy_availability(get_proxy())[0]:
                            break
                        else:
                            logger("proxies are not connectable. pls upgrade theme", 'p')
                            logger("we check proxy list again after 5 minute", 'p')
                            time.sleep(300)
            mr = MonthlyReport.objects.filter(is_finished=False, is_acceptable=True)
            for item in mr:
                while True:
                    try:
                        logger("getting " + str(item) + " monthly report data", 'p')
                        get_monthly_report_number(item, getattr(item, 'link'), get_proxy()['https'])
                        cc = Company.objects.get(monthly_report=item)
                        cc.save()
                        logger("getting " + str(item) + " monthly report has finished. we are waiting for " + str(get_time_sleep()) + " seconds",
                               'p')
                        time.sleep(get_time_sleep())
                        break
                    except Company.DoesNotExist as e:
                        logger("company problem : " + str(e), 'p')
                        break
                    except Exception as e:
                        logger("problem has happened during the event : " + str(e), 'p')
                        while True:
                            logger("checking proxy...", 'p')
                            if check_proxy_availability(get_proxy())[0]:
                                break
                            else:
                                logger("proxies are not connectable. pls upgrade theme", 'p')
                                logger("we check proxy list again after 5 minute", 'p')
                                time.sleep(300)
                while True:
                    try:
                        logger("check and update data with first page", 'p')
                        get_codal_data(codal_first_page(), get_proxy()['https'])
                        logger(str(get_time_sleep()) + "second delay after updating data with first page", 'p')
                        time.sleep(get_time_sleep())
                        break
                    except Company.DoesNotExist as e:
                        logger("company problem : " + str(e), 'p')
                        break
                    except Exception as e:
                        logger("problem has happened during the event : " + str(e), 'p')
                        while True:
                            logger("checking proxy...", 'p')
                            if check_proxy_availability(get_proxy())[0]:
                                break
                            else:
                                logger("proxies are not connectable. pls upgrade theme", 'p')
                                logger("we check proxy list again after 5 minute", 'p')
                                time.sleep(300)
            sr = SeasonalReport.objects.filter(is_finished=False, is_acceptable=True)
            for item in sr:
                while True:
                    try:
                        logger("getting " + str(item) + " seasonal report data", 'p')
                        get_seasonal_report_number(item, getattr(item, 'link'), get_proxy()['https'])
                        cc = Company.objects.get(seasonal_report=item)
                        cc.save()
                        logger("getting " + str(item) + " seasonal report has finished. we are waiting for " + str(get_time_sleep()) + " seconds",
                               'p')
                        time.sleep(get_time_sleep())
                        break
                    except Company.DoesNotExist as e:
                        logger("company problem : " + str(e), 'p')
                        break
                    except Exception as e:
                        logger("problem has happened during the event : " + str(e), 'p')
                        while True:
                            logger("checking proxy...", 'p')
                            if check_proxy_availability(get_proxy())[0]:
                                break
                            else:
                                logger("proxies are not connectable. pls upgrade theme", 'p')
                                logger("we check proxy list again after 5 minute", 'p')
                                time.sleep(300)
                while True:
                    try:
                        logger("check and update data with first page", 'p')
                        get_codal_data(codal_first_page(), get_proxy()['https'])
                        logger(str(get_time_sleep()) + "second delay after updating data with first page", 'p')
                        time.sleep(get_time_sleep())
                        break
                    except Company.DoesNotExist as e:
                        logger("company problem : " + str(e), 'p')
                        break
                    except Exception as e:
                        logger("problem has happened during the event : " + str(e), 'p')
                        while True:
                            logger("checking proxy...", 'p')
                            if check_proxy_availability(get_proxy())[0]:
                                break
                            else:
                                logger("proxies are not connectable. pls upgrade theme", 'p')
                                logger("we check proxy list again after 5 minute", 'p')
                                time.sleep(300)
            qq.delete()
            logger("---------------------------------- next -------------------------------", 'p')
        logger("the robot has finished task 1", 'p')
        logger("the robot has started task 2", 'p')
        while True:
            try:
                logger("check and update data with first page", 'p')
                get_codal_data(codal_first_page(), get_proxy()['https'])
                logger(str(get_time_sleep()) + "second delay after updating data with first page", 'p')
                time.sleep(get_time_sleep())
                break
            except Company.DoesNotExist as e:
                logger("company problem : " + str(e), 'p')
                break
            except Exception as e:
                logger("problem has happened during the event : " + str(e), 'p')
                while True:
                    logger("checking proxy...", 'p')
                    if check_proxy_availability(get_proxy())[0]:
                        break
                    else:
                        logger("proxies are not connectable. pls upgrade theme", 'p')
                        logger("we check proxy list again after 5 minute", 'p')
                        time.sleep(300)
        mr = MonthlyReport.objects.filter(is_finished=False, is_acceptable=True)
        for item in mr:
            while True:
                try:
                    logger("getting " + str(item) + " monthly report data", 'p')
                    get_monthly_report_number(item, getattr(item, 'link'), get_proxy()['https'])
                    cc = Company.objects.get(monthly_report=item)
                    cc.save()
                    logger("getting " + str(item) + " monthly report has finished. we are waiting for " + str(get_time_sleep()) + " seconds", 'p')
                    time.sleep(get_time_sleep())
                    break
                except Company.DoesNotExist as e:
                    logger("company problem : " + str(e), 'p')
                    break
                except Exception as e:
                    logger("problem has happened during the event : " + str(e), 'p')
                    while True:
                        logger("checking proxy...", 'p')
                        if check_proxy_availability(get_proxy())[0]:
                            break
                        else:
                            logger("proxies are not connectable. pls upgrade theme", 'p')
                            logger("we check proxy list again after 5 minute", 'p')
                            time.sleep(300)
            while True:
                try:
                    logger("check and update data with first page", 'p')
                    get_codal_data(codal_first_page(), get_proxy()['https'])
                    logger(str(get_time_sleep()) + "second delay after updating data with first page", 'p')
                    time.sleep(get_time_sleep())
                    break
                except Company.DoesNotExist as e:
                    logger("company problem : " + str(e), 'p')
                    break
                except Exception as e:
                    logger("problem has happened during the event : " + str(e), 'p')
                    while True:
                        logger("checking proxy...", 'p')
                        if check_proxy_availability(get_proxy())[0]:
                            break
                        else:
                            logger("proxies are not connectable. pls upgrade theme", 'p')
                            logger("we check proxy list again after 5 minute", 'p')
                            time.sleep(300)
        sr = SeasonalReport.objects.filter(is_finished=False, is_acceptable=True)
        for item in sr:
            while True:
                try:
                    logger("getting " + str(item) + " seasonal report data", 'p')
                    get_seasonal_report_number(item, getattr(item, 'link'), get_proxy()['https'])
                    cc = Company.objects.get(seasonal_report=item)
                    cc.save()
                    logger("getting " + str(item) + " seasonal report has finished. we are waiting for " + str(get_time_sleep()) + " seconds", 'p')
                    time.sleep(get_time_sleep())
                    break
                except Company.DoesNotExist as e:
                    logger("company problem : " + str(e), 'p')
                    break
                except Exception as e:
                    logger("problem has happened during the event : " + str(e), 'p')
                    while True:
                        logger("checking proxy...", 'p')
                        if check_proxy_availability(get_proxy())[0]:
                            break
                        else:
                            logger("proxies are not connectable. pls upgrade theme", 'p')
                            logger("we check proxy list again after 5 minute", 'p')
                            time.sleep(300)
            while True:
                try:
                    logger("check and update data with first page", 'p')
                    get_codal_data(codal_first_page(), get_proxy()['https'])
                    logger(str(get_time_sleep()) + "second delay after updating data with first page", 'p')
                    time.sleep(get_time_sleep())
                    break
                except Company.DoesNotExist as e:
                    logger("company problem : " + str(e), 'p')
                    break
                except Exception as e:
                    logger("problem has happened during the event : " + str(e), 'p')
                    while True:
                        logger("checking proxy...", 'p')
                        if check_proxy_availability(get_proxy())[0]:
                            break
                        else:
                            logger("proxies are not connectable. pls upgrade theme", 'p')
                            logger("we check proxy list again after 5 minute", 'p')
                            time.sleep(300)
        logger("the robot has finished task 2", 'p')



