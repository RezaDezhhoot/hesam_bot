import datetime
from django.db import models

from logger.views import logger


class MonthlyReport(models.Model):
    company_name = models.CharField(max_length=255, null=False, blank=False, verbose_name="نام شرکت مربوط به گزارش")
    name = models.CharField(max_length=255, null=False, blank=False, verbose_name="موضوع گزارش")
    link = models.URLField(max_length=2000, default="empty", verbose_name="لینک گزارش")
    reported_number = models.IntegerField(null=True, blank=True, verbose_name="عدد گزارش")
    reported_date = models.CharField(max_length=255, null=False, blank=False,
                                     verbose_name="گزارش فعالیت ماهانه دوره ۱ ماهه منتهی به تاریخ")
    reported_date_year = models.PositiveIntegerField(null=True, editable=False, verbose_name="سال گزارش")
    reported_date_month = models.PositiveIntegerField(null=True, editable=False, verbose_name="ماه گزارش")
    reported_date_day = models.PositiveIntegerField(null=True, editable=False, verbose_name="روز گزارش")
    created_at = models.DateTimeField(default=datetime.datetime.now(), verbose_name="تاریخ و زمان ایجاد")
    updated_at = models.DateTimeField(default=datetime.datetime.now(), verbose_name="تاریخ و زمان بروزرسانی")
    is_finished = models.BooleanField(default=False, verbose_name="آیا این گزارش کامل شده است؟")
    is_acceptable = models.BooleanField(default=True, verbose_name="آیا این گزارش با فرمت های طراحی شده سازگار است؟")

    class Meta:
        verbose_name = "گزارش مالی  ماهانه"
        verbose_name_plural = "گزارش مالی  ماهانه"

    def __str__(self):
        return str(self.company_name) + " - " + str(self.name)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        try:
            reported_date = self.reported_date
            reported_date = reported_date.replace("-", " ")
            reported_date = reported_date.split()
            self.reported_date_year = int(reported_date[0])
            self.reported_date_month = int(reported_date[1])
            self.reported_date_day = int(reported_date[2])
        except:
            pass
        super().save(*args, **kwargs)


class SeasonalReport(models.Model):
    company_name = models.CharField(max_length=255, null=False, blank=False, verbose_name="نام شرکت مربوط به گزارش")
    name = models.CharField(max_length=255, null=True, blank=True, verbose_name="موضوع گزارش")
    link = models.URLField(max_length=2000, default="empty", verbose_name="لینک گزارش")
    reported_percentage = models.FloatField(max_length=255, null=True, blank=True, verbose_name="درصد گزارش شده")
    reported_percentage_color = models.CharField(max_length=255, null=True, blank=True, verbose_name="رنگ درصد")
    reported_date = models.CharField(max_length=255, null=False, blank=False,
                                     verbose_name="گزارش فصلی سه ماهه منتهی به تاریخ")
    reported_date_year = models.PositiveIntegerField(null=True, editable=False, verbose_name="سال گزارش")
    reported_date_month = models.PositiveIntegerField(null=True, editable=False, verbose_name="ماه گزارش")
    reported_date_day = models.PositiveIntegerField(null=True, editable=False, verbose_name="روز گزارش")
    created_at = models.DateTimeField(default=datetime.datetime.now(), verbose_name="تاریخ و زمان ایجاد")
    updated_at = models.DateTimeField(default=datetime.datetime.now(), verbose_name="تاریخ و زمان بروزرسانی")
    is_finished = models.BooleanField(default=False, verbose_name="آیا این گزارش کامل شده است؟")
    is_acceptable = models.BooleanField(default=True, verbose_name="آیا این گزارش با فرمت های طراحی شده سازگار است؟")

    class Meta:
        verbose_name = "گزارش فصلی سه ماهه"
        verbose_name_plural = "گزارش فصلی سه ماهه"

    def __str__(self):
        return str(self.company_name) + " - " + str(self.name)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        try:
            reported_date = self.reported_date
            reported_date = reported_date.replace("-", " ")
            reported_date = reported_date.split()
            self.reported_date_year = int(reported_date[0])
            self.reported_date_month = int(reported_date[1])
            self.reported_date_day = int(reported_date[2])
        except:
            pass
        super().save(*args, **kwargs)


class Company(models.Model):
    badge = models.CharField(max_length=255, null=True, blank=True, verbose_name="نماد سازمان")
    badge_link = models.URLField(max_length=2000, null=True, blank=True, verbose_name="لینک جزئیات سازمان")
    name = models.CharField(max_length=255, null=True, blank=True, verbose_name="نام سازمان")
    monthly_report = models.ManyToManyField(MonthlyReport, blank=True, verbose_name="گزارشات ماهانه")
    seasonal_report = models.ManyToManyField(SeasonalReport, blank=True, verbose_name="گزارشات فصلی")
    created_at = models.DateTimeField(default=datetime.datetime.now(), verbose_name="تاریخ و زمان ایجاد")
    updated_at = models.DateTimeField(default=datetime.datetime.now(), verbose_name="تاریخ و زمان بروزرسانی")
    is_monthly_report_ready = models.BooleanField(default=False,
                                                  verbose_name="آیا برای این شرکت گزارشات ماهانه قابل ارائه است؟")
    is_seasonal_report_ready = models.BooleanField(default=False,
                                                   verbose_name="آیا برای این شرکت گزارشات فصلی قابل ارائه است؟")
    date_finished = models.DateField(null=True, blank=True, verbose_name="زمان پایان عملیات")
    monthly_report_1_date = models.CharField(max_length=255, null=True, blank=True, editable=False,
                                             verbose_name="تاریخ آخرین گزارش ماهانه موجود")
    monthly_report_2_date = models.CharField(max_length=255, null=True, blank=True, editable=False,
                                             verbose_name="تاریخ گزارش ماهانه یکی مانده به آخر")
    monthly_report_3_date = models.CharField(max_length=255, null=True, blank=True, editable=False,
                                             verbose_name="تاریخ گزارش ماهانه دوتا مانده به آخر")
    monthly_report_comparison_1_4 = models.FloatField(null=True, blank=True, editable=False,
                                                      verbose_name="مقایسه آخرین گزارش ماهانه موجود")
    monthly_report_comparison_2_5 = models.FloatField(null=True, blank=True, editable=False,
                                                      verbose_name="مقایسه گزارش ماهانه یکی مانده به آخر")
    monthly_report_comparison_3_6 = models.FloatField(null=True, blank=True, editable=False,
                                                      verbose_name="مقایسه گزارش ماهانه دوتا مانده به آخر")
    seasonal_report_spring_date = models.CharField(max_length=255, null=True, blank=True, editable=False,
                                                   verbose_name="تاریخ گزارش فصلی بهار")
    seasonal_report_spring_date_order = models.SmallIntegerField(editable=False, null=False, default=1)
    seasonal_report_spring_percentage = models.FloatField(null=True, blank=True, editable=False,
                                                         verbose_name="درصد گزارش فصلی بهار")
    seasonal_report_spring_color = models.CharField(max_length=255, null=True, blank=True, editable=False,
                                                    verbose_name="رنگ گزارش فصلی بهار")
    seasonal_report_summer_date = models.CharField(max_length=255, null=True, blank=True, editable=False,
                                                   verbose_name="تاریخ گزارش فصلی تابستان")
    seasonal_report_summer_date_order = models.SmallIntegerField(editable=False, null=False, default=2)
    seasonal_report_summer_percentage = models.FloatField(null=True, blank=True, editable=False,
                                                         verbose_name="درصد گزارش فصلی تابستان")
    seasonal_report_summer_color = models.CharField(max_length=255, null=True, blank=True, editable=False,
                                                    verbose_name="رنگ گزارش فصلی تابستان")
    seasonal_report_fall_date = models.CharField(max_length=255, null=True, blank=True, editable=False,
                                                 verbose_name="تاریخ گزارش فصلی پاییز")
    seasonal_report_fall_date_order = models.SmallIntegerField(editable=False, null=False, default=3)
    seasonal_report_fall_percentage = models.FloatField(null=True, blank=True, editable=False,
                                                       verbose_name="درصد گزارش فصلی پاییز")
    seasonal_report_fall_color = models.CharField(max_length=255, null=True, blank=True, editable=False,
                                                  verbose_name="رنگ گزارش فصلی پاییز")
    seasonal_report_winter_date = models.CharField(max_length=255, null=True, blank=True, editable=False,
                                                   verbose_name="تاریخ گزارش فصلی زمستان")
    seasonal_report_winter_date_order = models.SmallIntegerField(editable=False, null=False, default=4)
    seasonal_report_winter_percentage = models.FloatField(null=True, blank=True, editable=False,
                                                         verbose_name="درصد گزارش فصلی زمستان")
    seasonal_report_winter_color = models.CharField(max_length=255, null=True, blank=True, editable=False,
                                                    verbose_name="رنگ گزارش فصلی زمستان")

    class Meta:
        verbose_name = "سازمان های ارائه شده در کدال"
        verbose_name_plural = "سازمان های ارائه شده در کدال"

    def __str__(self):
        return str(self.name)

    def save(self, *args, **kwargs):
        # ----------------monthly report------------------#
        try:
            m1 = self.monthly_report.all().order_by('-reported_date')[0]
            logger("m1 - report name: " + str(m1), "d")
            x01 = m1.reported_number
            logger("m1 - reported_number: " + str(x01), "d")
            self.monthly_report_1_date = m1.reported_date
            logger("m1 - reported_date: " + str(m1.reported_date), "d")
            this_year = m1.reported_date_year
            logger("m1 - year: " + str(this_year), "d")
            this_month = m1.reported_date_month
            logger("m1 - month: " + str(this_month), "d")
            if this_month == 2:
                logger("m1 month is 2", "d")
                try:
                    m2 = self.monthly_report.filter(reported_date_year=this_year, reported_date_month=1)[0]
                    x02 = m2.reported_number
                    self.monthly_report_2_date = m2.reported_date
                except:
                    logger("مشکل در دریافت گزارش ماه یکی مانده به آخر امسال", "d")
                    x02 = 0
                try:
                    m3 = self.monthly_report.filter(reported_date_year=this_year - 1, reported_date_month=12)[0]
                    x03 = m3.reported_number
                    self.monthly_report_3_date = m3.reported_date
                except:
                    logger("مشکل در دریافت گزارش ماه دوتا مانده به آخر امسال", "d")
                    x03 = 0
                try:
                    m4 = self.monthly_report.filter(reported_date_year=this_year - 1, reported_date_month=2)[0]
                    x04 = m4.reported_number
                except:
                    logger("مشکل در دریافت گزارش ماه آخر سال قبل", "d")
                    x04 = 0
                try:
                    m5 = self.monthly_report.filter(reported_date_year=this_year - 1, reported_date_month=1)[0]
                    x05 = m5.reported_number
                except:
                    logger("مشکل در دریافت گزارش ماه یکی مانده به آخر سال قبل", "d")
                    x05 = 0
                try:
                    m6 = self.monthly_report.filter(reported_date_year=this_year - 2, reported_date_month=12)[0]
                    x06 = m6.reported_number
                except:
                    logger("مشکل در دریافت گزارش ماه دوتا مانده به آخر سال قبل", "d")
                    x06 = 0
            elif this_month == 1:
                logger("m1 month is 1", "d")
                try:
                    m2 = self.monthly_report.filter(reported_date_year=this_year - 1,
                                                    reported_date_month=this_month - 12)[0]
                    x02 = m2.reported_number
                    self.monthly_report_2_date = m2.reported_date
                except:
                    logger("مشکل در دریافت گزارش ماه یکی مانده به آخر امسال", "d")
                    x02 = 0
                try:
                    m3 = \
                        self.monthly_report.filter(reported_date_year=this_year - 1,
                                                   reported_date_month=this_month - 11)[0]
                    x03 = m3.reported_number
                    self.monthly_report_3_date = m3.reported_date
                except:
                    logger("مشکل در دریافت گزارش ماه دوتا مانده به آخر امسال", "d")
                    x03 = 0
                try:
                    m4 = \
                        self.monthly_report.filter(reported_date_year=this_year - 1, reported_date_month=1)[0]
                    x04 = m4.reported_number
                except:
                    logger("مشکل در دریافت گزارش ماه آخر سال قبل", "d")
                    x04 = 0
                try:
                    m5 = \
                        self.monthly_report.filter(reported_date_year=this_year - 2, reported_date_month=12)[0]
                    x05 = m5.reported_number
                except:
                    logger("مشکل در دریافت گزارش ماه یکی مانده به آخر سال قبل", "d")
                    x05 = 0
                try:
                    m6 = \
                        self.monthly_report.filter(reported_date_year=this_year - 2, reported_date_month=11)[0]
                    x06 = m6.reported_number
                except:
                    logger("مشکل در دریافت گزارش ماه دوتا مانده به آخر سال قبل", "d")
                    x06 = 0
            else:
                logger("m1 month is not 1 or 2", "d")
                try:
                    m2 = self.monthly_report.filter(reported_date_year=this_year, reported_date_month=this_month - 1)[0]
                    logger("m2 - report name: " + str(m2), "d")
                    x02 = m2.reported_number
                    logger("m2 - reported_number: " + str(x02), "d")
                    self.monthly_report_2_date = m2.reported_date
                    logger("m2 - reported_date: " + str(m2.reported_date), "d")
                except Exception as e:
                    logger("m2 exception: " + str(e), "d")
                    logger("مشکل در دریافت گزارش ماه یکی مانده به آخر امسال", "d")
                    x02 = 0
                try:
                    m3 = \
                        self.monthly_report.filter(reported_date_year=this_year,
                                                   reported_date_month=this_month - 2)[0]
                    x03 = m3.reported_number
                    self.monthly_report_3_date = m3.reported_date
                except Exception as e:
                    logger("m2 exception: " + str(e), "d")
                    logger("مشکل در دریافت گزارش ماه دوتا مانده به آخر امسال", "d")
                    x03 = 0
                try:
                    m4 = \
                        self.monthly_report.filter(reported_date_year=this_year - 1,
                                                   reported_date_month=this_month)[0]
                    x04 = m4.reported_number
                except Exception as e:
                    logger("m2 exception: " + str(e), "d")
                    logger("مشکل در دریافت گزارش ماه آخر سال قبل", "d")
                    x04 = 0
                try:
                    m5 = \
                        self.monthly_report.filter(reported_date_year=this_year - 1,
                                                   reported_date_month=this_month - 1)[0]
                    x05 = m5.reported_number
                except Exception as e:
                    logger("m2 exception: " + str(e), "d")
                    logger("مشکل در دریافت گزارش ماه یکی مانده به آخر سال قبل", "d")
                    x05 = 0
                try:
                    m6 = \
                        self.monthly_report.filter(reported_date_year=this_year - 1,
                                                   reported_date_month=this_month - 2)[0]
                    x06 = m6.reported_number
                except Exception as e:
                    logger("m2 exception: " + str(e), "d")
                    logger("مشکل در دریافت گزارش ماه دوتا مانده به آخر سال قبل", "d")
                    x06 = 0
            logger("", 'd')
            logger("---------------- monthly report calculation start------------------", 'd')
            try:
                c1 = ((x01 / x04) - 1) * 100
                c1 = round(c1, 2)
                logger("c1: " + str(c1), "d")
                self.monthly_report_comparison_1_4 = c1
            except Exception as e:
                logger("c1 cant calculate because: " + str(e), "d")
                self.monthly_report_comparison_1_4 = None
            try:
                c2 = ((x02 / x05) - 1) * 100
                c2 = round(c2, 2)
                logger("c2: " + str(c2), "d")
                self.monthly_report_comparison_2_5 = c2
            except Exception as e:
                logger("c2 cant calculate because: " + str(e), "d")
                self.monthly_report_comparison_2_5 = None
            try:
                c3 = ((x03 / x06) - 1) * 100
                c3 = round(c3, 2)
                logger("c3: " + str(c3), "d")
                self.monthly_report_comparison_3_6 = c3
            except Exception as e:
                self.monthly_report_comparison_3_6 = None
                logger("c3 cant calculate because: " + str(e), "d")

            logger("---------------- monthly report calculation end------------------\n", 'd')
            if self.monthly_report_comparison_1_4 is None:
                logger("self.monthly_report_comparison_1_4: None ", "d")
            else:
                logger("self.monthly_report_comparison_1_4: " + str(self.monthly_report_comparison_1_4), "d")

            if self.monthly_report_comparison_2_5 is None:
                logger("self.monthly_report_comparison_2_5: None ", "d")
            else:
                logger("self.monthly_report_comparison_2_5: " + str(self.monthly_report_comparison_2_5), "d")

            if self.monthly_report_comparison_3_6 is None:
                logger("self.monthly_report_comparison_3_6: None ", "d")
            else:
                logger("self.monthly_report_comparison_3_6: " + str(self.monthly_report_comparison_3_6), "d")

            if self.monthly_report_comparison_1_4 and self.monthly_report_comparison_2_5 and self.monthly_report_comparison_3_6 is not None:
                self.is_monthly_report_ready = True
                logger("is all report ok?: " + "True", "d")
            else:
                self.is_monthly_report_ready = False
                logger("is all report ok?: " + "False", "d")
        except Exception as e:
            logger("this is monthly main exception: " + str(e), "d")
            pass
        logger("---------------------------------------------------------", "d")
        # ----------------seasonal report------------------#
        try:
            m1 = self.seasonal_report.all().order_by('-reported_date')[0]
            logger("m1 reported name: " + str(m1), "d")
            this_month = m1.reported_date_month
            logger("m1 reported month: " + str(this_month), "d")
            if this_month == 1 or this_month == 2 or this_month == 3:
                logger("this is: " + "1 or 2 or 3", "d")
                self.seasonal_report_spring_percentage = float(m1.reported_percentage)
                self.seasonal_report_spring_color = str(m1.reported_percentage_color)
                self.seasonal_report_spring_date = str(m1.reported_date)
                logger("m1 reported spring date: " + self.seasonal_report_spring_date, "d")
                logger("m1 reported spring percentage: " + str(self.seasonal_report_spring_percentage), "d")
                logger("m1 reported spring color: " + self.seasonal_report_spring_color, "d")
                m2_1 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                   reported_date_month='12')
                if m2_1.count() == 0:
                    logger("m2_1 is empty", "d")
                    m2_2 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                       reported_date_month='11')
                    if m2_2.count() == 0:
                        logger("m2_2 is empty", "d")
                        m2_3 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                           reported_date_month='10')
                        if m2_3.count() == 0:
                            logger("m2_3 is empty", "d")
                        else:
                            try:
                                logger("m2_3: try lvl 3", "d")
                                m2_3 = m2_3[0]
                                logger("m2_3 reported name: " + str(m2_3), "d")
                                self.seasonal_report_winter_percentage = float(m2_3.reported_percentage)
                                self.seasonal_report_winter_color = str(m2_3.reported_percentage_color)
                                self.seasonal_report_winter_date = str(m2_3.reported_date)
                                logger("m2 reported winter date: " + self.seasonal_report_winter_date, "d")
                                logger("m2 reported winter percentage: " + str(self.seasonal_report_winter_percentage), "d")
                                logger("m2 reported winter color: " + self.seasonal_report_winter_color, "d")
                            except:
                                pass
                    else:
                        try:
                            logger("m2_2: try lvl 2", "d")
                            m2_2 = m2_2[0]
                            logger("m2_2 reported name: " + str(m2_2), "d")
                            self.seasonal_report_winter_percentage = float(m2_2.reported_percentage)
                            self.seasonal_report_winter_color = str(m2_2.reported_percentage_color)
                            self.seasonal_report_winter_date = str(m2_2.reported_date)
                            logger("m2 reported winter date: " + self.seasonal_report_winter_date, "d")
                            logger("m2 reported winter percentage: " + str(self.seasonal_report_winter_percentage), "d")
                            logger("m2 reported winter color: " + self.seasonal_report_winter_color, "d")
                        except Exception as e:
                            logger("m2: try lvl 2 exception: " + str(e), "d")
                else:
                    try:
                        logger("m2_1: try lvl 1", "d")
                        m2_1 = m2_1[0]
                        logger("m2_1 reported name: " + str(m2_1), "d")
                        self.seasonal_report_winter_percentage = float(m2_1.reported_percentage)
                        self.seasonal_report_winter_color = str(m2_1.reported_percentage_color)
                        self.seasonal_report_winter_date = str(m2_1.reported_date)
                        logger("m2 reported winter date: " + self.seasonal_report_winter_date, "d")
                        logger("m2 reported winter percentage: " + str(self.seasonal_report_winter_percentage), "d")
                        logger("m2 reported winter color: " + self.seasonal_report_winter_color, "d")
                    except Exception as e:
                        logger("m2: try lvl 1 exception: " + str(e), "d")

                m3_1 = \
                    self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1, reported_date_month='9')
                if m3_1.count() == 0:
                    logger("m3_1 is empty", "d")
                    m3_2 = \
                        self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                    reported_date_month='8')
                    if m3_2.count() == 0:
                        logger("m3_2 is empty", "d")
                        m3_3 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                           reported_date_month='7')
                        if m3_3.count() == 0:
                            logger("m3_3 is empty", "d")
                        else:
                            try:
                                logger("m3_3: try lvl 3", "d")
                                m3_3 = m3_3[0]
                                logger("m3_3 reported name: " + str(m3_3), "d")
                                self.seasonal_report_fall_percentage = float(m3_3.reported_percentage)
                                self.seasonal_report_fall_color = str(m3_3.reported_percentage_color)
                                self.seasonal_report_fall_date = str(m3_3.reported_date)
                                logger("m3 reported fall date: " + self.seasonal_report_fall_date, "d")
                                logger("m3 reported fall percentage: " + str(self.seasonal_report_fall_percentage), "d")
                                logger("m3 reported fall color: " + self.seasonal_report_fall_color, "d")
                            except:
                                pass
                    else:
                        try:
                            logger("m3_2: try lvl 2", "d")
                            m3_2 = m3_2[0]
                            logger("m3_2 reported name: " + str(m3_2), "d")
                            self.seasonal_report_fall_percentage = float(m3_2.reported_percentage)
                            self.seasonal_report_fall_color = str(m3_2.reported_percentage_color)
                            self.seasonal_report_fall_date = str(m3_2.reported_date)
                            logger("m3 reported fall date: " + self.seasonal_report_fall_date, "d")
                            logger("m3 reported fall percentage: " + str(self.seasonal_report_fall_percentage), "d")
                            logger("m3 reported fall color: " + self.seasonal_report_fall_color, "d")
                        except Exception as e:
                            logger("m3: try lvl 2 exception: " + str(e), "d")
                else:
                    try:
                        logger("m3_1: try lvl 1", "d")
                        m3_1 = m3_1[0]
                        logger("m3_1 reported name: " + str(m3_1), "d")
                        self.seasonal_report_fall_percentage = float(m3_1.reported_percentage)
                        self.seasonal_report_fall_color = str(m3_1.reported_percentage_color)
                        self.seasonal_report_fall_date = str(m3_1.reported_date)
                        logger("m3 reported fall date: " + self.seasonal_report_fall_date, "d")
                        logger("m3 reported fall percentage: " + str(self.seasonal_report_fall_percentage), "d")
                        logger("m3 reported fall color: " + self.seasonal_report_fall_color, "d")
                    except Exception as e:
                        logger("m3: try lvl 1 exception: " + str(e), "d")
                m4_1 = \
                    self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1, reported_date_month='6')
                if m4_1.count() == 0:
                    logger("m4_1 is empty", "d")
                    m4_2 = \
                        self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                    reported_date_month='5')
                    if m4_2.count() == 0:
                        logger("m4_2 is empty", "d")
                        m4_3 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                           reported_date_month='4')
                        if m4_3.count() == 0:
                            logger("m4_3 is empty", "d")
                        else:
                            try:
                                logger("m4_3: try lvl 3", "d")
                                m4_3 = m4_3[0]
                                logger("m4_3 reported name: " + str(m4_3), "d")
                                self.seasonal_report_summer_percentage = float(m4_3.reported_percentage)
                                self.seasonal_report_summer_color = str(m4_3.reported_percentage_color)
                                self.seasonal_report_summer_date = str(m4_3.reported_date)
                                logger("m4 reported summer date: " + self.seasonal_report_summer_date, "d")
                                logger("m4 reported summer percentage: " + str(self.seasonal_report_summer_percentage), "d")
                                logger("m4 reported summer color: " + self.seasonal_report_summer_color, "d")
                            except:
                                pass
                    else:
                        try:
                            logger("m4_2: try lvl 2", "d")
                            m4_2 = m4_2[0]
                            logger("m4_2 reported name: " + str(m4_2), "d")
                            self.seasonal_report_summer_percentage = float(m4_2.reported_percentage)
                            self.seasonal_report_summer_color = str(m4_2.reported_percentage_color)
                            self.seasonal_report_summer_date = str(m4_2.reported_date)
                            logger("m4 reported summer date: " + self.seasonal_report_summer_date, "d")
                            logger("m4 reported summer percentage: " + str(self.seasonal_report_summer_percentage), "d")
                            logger("m4 reported summer color: " + self.seasonal_report_summer_color, "d")
                        except Exception as e:
                            logger("m4: try lvl 2 exception: " + str(e), "d")
                else:
                    try:
                        logger("m4_1: try lvl 1", "d")
                        m4_1 = m4_1[0]
                        logger("m4_1 reported name: " + str(m4_1), "d")
                        self.seasonal_report_summer_percentage = float(m4_1.reported_percentage)
                        self.seasonal_report_summer_color = str(m4_1.reported_percentage_color)
                        self.seasonal_report_summer_date = str(m4_1.reported_date)
                        logger("m4 reported summer date: " + self.seasonal_report_summer_date, "d")
                        logger("m4 reported summer percentage: " + str(self.seasonal_report_summer_percentage), "d")
                        logger("m4 reported summer color: " + self.seasonal_report_summer_color, "d")
                    except Exception as e:
                        logger("m4: try lvl 1 exception: " + str(e), "d")
            elif this_month == 4 or this_month == 5 or this_month == 6:
                logger("this is: " + "4 or 5 or 6", "d")
                self.seasonal_report_summer_percentage = float(m1.reported_percentage)
                self.seasonal_report_summer_color = str(m1.reported_percentage_color)
                self.seasonal_report_summer_date = str(m1.reported_date)
                logger("m1 reported summer date: " + self.seasonal_report_summer_date, "d")
                logger("m1 reported summer percentage: " + str(self.seasonal_report_summer_percentage), "d")
                logger("m1 reported summer color: " + self.seasonal_report_summer_color, "d")
                m2_1 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year,
                                                   reported_date_month='3')
                if m2_1.count() == 0:
                    logger("m2_1 is empty", "d")
                    m2_2 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year,
                                                       reported_date_month='2')
                    if m2_2.count() == 0:
                        logger("m2_2 is empty", "d")
                        m2_3 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year,
                                                           reported_date_month='1')
                        if m2_3.count() == 0:
                            logger("m2_3 is empty", "d")
                        else:
                            try:
                                logger("m2_3: try lvl 3", "d")
                                m2_3 = m2_3[0]
                                logger("m2_3 reported name: " + str(m2_3), "d")
                                self.seasonal_report_spring_percentage = float(m2_3.reported_percentage)
                                self.seasonal_report_spring_color = str(m2_3.reported_percentage_color)
                                self.seasonal_report_spring_date = str(m2_3.reported_date)
                                logger("m2 reported spring date: " + self.seasonal_report_spring_date, "d")
                                logger(
                                    "m2 reported spring percentage: " + str(self.seasonal_report_spring_percentage), "d")
                                logger("m2 reported spring color: " + self.seasonal_report_spring_color, "d")
                            except:
                                pass
                    else:
                        try:
                            logger("m2_2: try lvl 2", "d")
                            m2_2 = m2_2[0]
                            logger("m2_2 reported name: " + str(m2_2), "d")
                            self.seasonal_report_spring_percentage = float(m2_2.reported_percentage)
                            self.seasonal_report_spring_color = str(m2_2.reported_percentage_color)
                            self.seasonal_report_spring_date = str(m2_2.reported_date)
                            logger("m2 reported spring date: " + self.seasonal_report_spring_date, "d")
                            logger(
                                "m2 reported spring percentage: " + str(self.seasonal_report_spring_percentage), "d")
                            logger("m2 reported spring color: " + self.seasonal_report_spring_color, "d")
                        except Exception as e:
                            logger("m2: try lvl 2 exception: " + str(e), "d")
                else:
                    try:
                        logger("m2_1: try lvl 1", "d")
                        m2_1 = m2_1[0]
                        logger("m2_1 reported name: " + str(m2_1), "d")
                        self.seasonal_report_spring_percentage = float(m2_1.reported_percentage)
                        self.seasonal_report_spring_color = str(m2_1.reported_percentage_color)
                        self.seasonal_report_spring_date = str(m2_1.reported_date)
                        logger("m2 reported spring date: " + self.seasonal_report_spring_date, "d")
                        logger("m2 reported spring percentage: " + str(self.seasonal_report_spring_percentage), "d")
                        logger("m2 reported spring color: " + self.seasonal_report_spring_color, "d")
                    except Exception as e:
                        logger("m2: try lvl 1 exception: " + str(e), "d")

                m3_1 = \
                    self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                reported_date_month='12')
                if m3_1.count() == 0:
                    logger("m3_1 is empty", "d")
                    m3_2 = \
                        self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                    reported_date_month='11')
                    if m3_2.count() == 0:
                        logger("m3_2 is empty", "d")
                        m3_3 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                           reported_date_month='10')
                        if m3_3.count() == 0:
                            logger("m3_3 is empty", "d")
                        else:
                            try:
                                logger("m3_3: try lvl 3", "d")
                                m3_3 = m3_3[0]
                                logger("m3_3 reported name: " + str(m3_3), "d")
                                self.seasonal_report_winter_percentage = float(m3_3.reported_percentage)
                                self.seasonal_report_winter_color = str(m3_3.reported_percentage_color)
                                self.seasonal_report_winter_date = str(m3_3.reported_date)
                                logger("m3 reported winter date: " + self.seasonal_report_winter_date, "d")
                                logger(
                                    "m3 reported winter percentage: " + str(self.seasonal_report_winter_percentage), "d")
                                logger("m3 reported winter color: " + self.seasonal_report_winter_color, "d")
                            except:
                                pass
                    else:
                        try:
                            logger("m3_2: try lvl 2", "d")
                            m3_2 = m3_2[0]
                            logger("m3_2 reported name: " + str(m3_2), "d")
                            self.seasonal_report_winter_percentage = float(m3_2.reported_percentage)
                            self.seasonal_report_winter_color = str(m3_2.reported_percentage_color)
                            self.seasonal_report_winter_date = str(m3_2.reported_date)
                            logger("m3 reported winter date: " + self.seasonal_report_winter_date, "d")
                            logger("m3 reported winter percentage: " + str(self.seasonal_report_winter_percentage), "d")
                            logger("m3 reported winter color: " + self.seasonal_report_winter_color, "d")
                        except Exception as e:
                            logger("m3: try lvl 2 exception: " + str(e), "d")
                else:
                    try:
                        logger("m3_1: try lvl 1", "d")
                        m3_1 = m3_1[0]
                        logger("m3_1 reported name: " + str(m3_1), "d")
                        self.seasonal_report_winter_percentage = float(m3_1.reported_percentage)
                        self.seasonal_report_winter_color = str(m3_1.reported_percentage_color)
                        self.seasonal_report_winter_date = str(m3_1.reported_date)
                        logger("m3 reported winter date: " + self.seasonal_report_winter_date, "d")
                        logger("m3 reported winter percentage: " + str(self.seasonal_report_winter_percentage), "d")
                        logger("m3 reported winter color: " + self.seasonal_report_winter_color, "d")
                    except Exception as e:
                        logger("m3: try lvl 1 exception: " + str(e), "d")
                m4_1 = \
                    self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                reported_date_month='9')
                if m4_1.count() == 0:
                    logger("m4_1 is empty", "d")
                    m4_2 = \
                        self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                    reported_date_month='8')
                    if m4_2.count() == 0:
                        logger("m4_2 is empty", "d")
                        m4_3 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                           reported_date_month='7')
                        if m4_3.count() == 0:
                            logger("m4_3 is empty", "d")
                        else:
                            try:
                                logger("m4_3: try lvl 3", "d")
                                m4_3 = m4_3[0]
                                logger("m4_3 reported name: " + str(m4_3), "d")
                                self.seasonal_report_fall_percentage = float(m4_3.reported_percentage)
                                self.seasonal_report_fall_color = str(m4_3.reported_percentage_color)
                                self.seasonal_report_fall_date = str(m4_3.reported_date)
                                logger("m4 reported fall date: " + self.seasonal_report_fall_date, "d")
                                logger(
                                    "m4 reported fall percentage: " + str(self.seasonal_report_fall_percentage), "d")
                                logger("m4 reported fall color: " + self.seasonal_report_fall_color, "d")
                            except:
                                pass
                    else:
                        try:
                            logger("m4_2: try lvl 2", "d")
                            m4_2 = m4_2[0]
                            logger("m4_2 reported name: " + str(m4_2), "d")
                            self.seasonal_report_fall_percentage = float(m4_2.reported_percentage)
                            self.seasonal_report_fall_color = str(m4_2.reported_percentage_color)
                            self.seasonal_report_fall_date = str(m4_2.reported_date)
                            logger("m4 reported fall date: " + self.seasonal_report_fall_date, "d")
                            logger(
                                "m4 reported fall percentage: " + str(self.seasonal_report_fall_percentage), "d")
                            logger("m4 reported fall color: " + self.seasonal_report_fall_color, "d")
                        except Exception as e:
                            logger("m4: try lvl 2 exception: " + str(e), "d")
                else:
                    try:
                        logger("m4_1: try lvl 1", "d")
                        m4_1 = m4_1[0]
                        logger("m4_1 reported name: " + str(m4_1), "d")
                        self.seasonal_report_fall_percentage = float(m4_1.reported_percentage)
                        self.seasonal_report_fall_color = str(m4_1.reported_percentage_color)
                        self.seasonal_report_fall_date = str(m4_1.reported_date)
                        logger("m4 reported fall date: " + self.seasonal_report_fall_date, "d")
                        logger("m4 reported fall percentage: " + str(self.seasonal_report_fall_percentage), "d")
                        logger("m4 reported fall color: " + self.seasonal_report_fall_color, "d")
                    except Exception as e:
                        logger("m4: try lvl 1 exception: " + str(e), "d")
            elif this_month == 7 or this_month == 8 or this_month == 9:
                logger("this is: " + "7 or 8 or 9", "d")
                self.seasonal_report_fall_percentage = float(m1.reported_percentage)
                self.seasonal_report_fall_color = str(m1.reported_percentage_color)
                self.seasonal_report_fall_date = str(m1.reported_date)
                logger("m1 reported fall date: " + self.seasonal_report_fall_date, "d")
                logger("m1 reported fall percentage: " + str(self.seasonal_report_fall_percentage), "d")
                logger("m1 reported fall color: " + self.seasonal_report_fall_color, "d")
                m2_1 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year,
                                                   reported_date_month='6')
                if m2_1.count() == 0:
                    logger("m2_1 is empty", "d")
                    m2_2 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year,
                                                       reported_date_month='5')
                    if m2_2.count() == 0:
                        logger("m2_2 is empty", "d")
                        m2_3 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year,
                                                           reported_date_month='4')
                        if m2_3.count() == 0:
                            logger("m2_3 is empty", "d")
                        else:
                            try:
                                logger("m2_3: try lvl 3", "d")
                                m2_3 = m2_3[0]
                                logger("m2_3 reported name: " + str(m2_3), "d")
                                self.seasonal_report_summer_percentage = float(m2_3.reported_percentage)
                                self.seasonal_report_summer_color = str(m2_3.reported_percentage_color)
                                self.seasonal_report_summer_date = str(m2_3.reported_date)
                                logger("m2 reported summer date: " + self.seasonal_report_summer_date, "d")
                                logger(
                                    "m2 reported summer percentage: " + str(self.seasonal_report_summer_percentage), "d")
                                logger("m2 reported summer color: " + self.seasonal_report_summer_color, "d")
                            except:
                                pass
                    else:
                        try:
                            logger("m2_2: try lvl 2", "d")
                            m2_2 = m2_2[0]
                            logger("m2_2 reported name: " + str(m2_2), "d")
                            self.seasonal_report_summer_percentage = float(m2_2.reported_percentage)
                            self.seasonal_report_summer_color = str(m2_2.reported_percentage_color)
                            self.seasonal_report_summer_date = str(m2_2.reported_date)
                            logger("m2 reported summer date: " + self.seasonal_report_summer_date, "d")
                            logger(
                                "m2 reported summer percentage: " + str(self.seasonal_report_summer_percentage), "d")
                            logger("m2 reported summer color: " + self.seasonal_report_summer_color, "d")
                        except Exception as e:
                            logger("m2: try lvl 2 exception: " + str(e), "d")
                else:
                    try:
                        logger("m2_1: try lvl 1", "d")
                        m2_1 = m2_1[0]
                        logger("m2_1 reported name: " + str(m2_1), "d")
                        self.seasonal_report_summer_percentage = float(m2_1.reported_percentage)
                        self.seasonal_report_summer_color = str(m2_1.reported_percentage_color)
                        self.seasonal_report_summer_date = str(m2_1.reported_date)
                        logger("m2 reported summer date: " + self.seasonal_report_summer_date, "d")
                        logger("m2 reported summer percentage: " + str(self.seasonal_report_summer_percentage), "d")
                        logger("m2 reported summer color: " + self.seasonal_report_summer_color, "d")
                    except Exception as e:
                        logger("m2: try lvl 1 exception: " + str(e), "d")

                m3_1 = \
                    self.seasonal_report.filter(reported_date_year=m1.reported_date_year,
                                                reported_date_month='3')
                if m3_1.count() == 0:
                    logger("m3_1 is empty", "d")
                    m3_2 = \
                        self.seasonal_report.filter(reported_date_year=m1.reported_date_year,
                                                    reported_date_month='2')
                    if m3_2.count() == 0:
                        logger("m3_2 is empty", "d")
                        m3_3 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year,
                                                           reported_date_month='1')
                        if m3_3.count() == 0:
                            logger("m3_3 is empty", "d")
                        else:
                            try:
                                logger("m3_3: try lvl 3", "d")
                                m3_3 = m3_3[0]
                                logger("m3_3 reported name: " + str(m3_3), "d")
                                self.seasonal_report_spring_percentage = float(m3_3.reported_percentage)
                                self.seasonal_report_spring_color = str(m3_3.reported_percentage_color)
                                self.seasonal_report_spring_date = str(m3_3.reported_date)
                                logger("m3 reported spring date: " + self.seasonal_report_spring_date, "d")
                                logger(
                                    "m3 reported spring percentage: " + str(self.seasonal_report_spring_percentage), "d")
                                logger("m3 reported spring color: " + self.seasonal_report_spring_color, "d")
                            except:
                                pass
                    else:
                        try:
                            logger("m3_2: try lvl 2", "d")
                            m3_2 = m3_2[0]
                            logger("m3_2 reported name: " + str(m3_2), "d")
                            self.seasonal_report_spring_percentage = float(m3_2.reported_percentage)
                            self.seasonal_report_spring_color = str(m3_2.reported_percentage_color)
                            self.seasonal_report_spring_date = str(m3_2.reported_date)
                            logger("m3 reported spring date: " + self.seasonal_report_spring_date, "d")
                            logger("m3 reported spring percentage: " + str(self.seasonal_report_spring_percentage), "d")
                            logger("m3 reported spring color: " + self.seasonal_report_spring_color, "d")
                        except Exception as e:
                            logger("m3: try lvl 2 exception: " + str(e), "d")
                else:
                    try:
                        logger("m3_1: try lvl 1", "d")
                        m3_1 = m3_1[0]
                        logger("m3_1 reported name: " + str(m3_1), "d")
                        self.seasonal_report_spring_percentage = float(m3_1.reported_percentage)
                        self.seasonal_report_spring_color = str(m3_1.reported_percentage_color)
                        self.seasonal_report_spring_date = str(m3_1.reported_date)
                        logger("m3 reported spring date: " + self.seasonal_report_spring_date, "d")
                        logger("m3 reported spring percentage: " + str(self.seasonal_report_spring_percentage), "d")
                        logger("m3 reported spring color: " + self.seasonal_report_spring_color, "d")
                    except Exception as e:
                        logger("m3: try lvl 1 exception: " + str(e), "d")
                m4_1 = \
                    self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                reported_date_month='12')
                if m4_1.count() == 0:
                    logger("m4_1 is empty", "d")
                    m4_2 = \
                        self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                    reported_date_month='11')
                    if m4_2.count() == 0:
                        logger("m4_2 is empty", "d")
                        m4_3 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year - 1,
                                                           reported_date_month='10')
                        if m4_3.count() == 0:
                            logger("m4_3 is empty", "d")
                        else:
                            try:
                                logger("m4_3: try lvl 3", "d")
                                m4_3 = m4_3[0]
                                logger("m4_3 reported name: " + str(m4_3), "d")
                                self.seasonal_report_winter_percentage = float(m4_3.reported_percentage)
                                self.seasonal_report_winter_color = str(m4_3.reported_percentage_color)
                                self.seasonal_report_winter_date = str(m4_3.reported_date)
                                logger("m4 reported winter date: " + self.seasonal_report_winter_date, "d")
                                logger(
                                    "m4 reported winter percentage: " + str(self.seasonal_report_winter_percentage), "d")
                                logger("m4 reported winter color: " + self.seasonal_report_winter_color, "d")
                            except:
                                pass
                    else:
                        try:
                            logger("m4_2: try lvl 2", "d")
                            m4_2 = m4_2[0]
                            logger("m4_2 reported name: " + str(m4_2), "d")
                            self.seasonal_report_winter_percentage = float(m4_2.reported_percentage)
                            self.seasonal_report_winter_color = str(m4_2.reported_percentage_color)
                            self.seasonal_report_winter_date = str(m4_2.reported_date)
                            logger("m4 reported winter date: " + self.seasonal_report_winter_date, "d")
                            logger(
                                "m4 reported winter percentage: " + str(self.seasonal_report_winter_percentage), "d")
                            logger("m4 reported winter color: " + self.seasonal_report_winter_color, "d")
                        except Exception as e:
                            logger("m4: try lvl 2 exception: " + str(e), "d")
                else:
                    try:
                        logger("m4_1: try lvl 1", "d")
                        m4_1 = m4_1[0]
                        logger("m4_1 reported name: " + str(m4_1), "d")
                        self.seasonal_report_winter_percentage = float(m4_1.reported_percentage)
                        self.seasonal_report_winter_color = str(m4_1.reported_percentage_color)
                        self.seasonal_report_winter_date = str(m4_1.reported_date)
                        logger("m4 reported winter date: " + self.seasonal_report_winter_date, "d")
                        logger("m4 reported winter percentage: " + str(self.seasonal_report_winter_percentage), "d")
                        logger("m4 reported winter color: " + self.seasonal_report_winter_color, "d")
                    except Exception as e:
                        logger("m4: try lvl 1 exception: " + str(e), "d")
            elif this_month == 10 or this_month == 11 or this_month == 12:
                logger("this is: " + "10 or 11 or 12", "d")
                self.seasonal_report_winter_percentage = float(m1.reported_percentage)
                self.seasonal_report_winter_color = str(m1.reported_percentage_color)
                self.seasonal_report_winter_date = str(m1.reported_date)
                logger("m1 reported winter date: " + self.seasonal_report_winter_date, "d")
                logger("m1 reported winter percentage: " + str(self.seasonal_report_winter_percentage), "d")
                logger("m1 reported winter color: " + self.seasonal_report_winter_color, "d")
                m2_1 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year, reported_date_month='9')
                if m2_1.count() == 0:
                    logger("m2_1 is empty", "d")
                    m2_2 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year, reported_date_month='8')
                    if m2_2.count() == 0:
                        logger("m2_2 is empty", "d")
                        m2_3 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year, reported_date_month='7')
                        if m2_3.count() == 0:
                            logger("m2_3 is empty", "d")
                        else:
                            try:
                                logger("m2_3: try lvl 3", "d")
                                m2_3 = m2_3[0]
                                logger("m2_3 reported name: " + str(m2_3), "d")
                                self.seasonal_report_fall_percentage = float(m2_3.reported_percentage)
                                self.seasonal_report_fall_color = str(m2_3.reported_percentage_color)
                                self.seasonal_report_fall_date = str(m2_3.reported_date)
                                logger("m2 reported fall date: " + self.seasonal_report_fall_date, "d")
                                logger(
                                    "m2 reported fall percentage: " + str(self.seasonal_report_fall_percentage), "d")
                                logger("m2 reported fall color: " + self.seasonal_report_fall_color, "d")
                            except:
                                pass
                    else:
                        try:
                            logger("m2_2: try lvl 2", "d")
                            m2_2 = m2_2[0]
                            logger("m2_2 reported name: " + str(m2_2), "d")
                            self.seasonal_report_fall_percentage = float(m2_2.reported_percentage)
                            self.seasonal_report_fall_color = str(m2_2.reported_percentage_color)
                            self.seasonal_report_fall_date = str(m2_2.reported_date)
                            logger("m2 reported fall date: " + self.seasonal_report_fall_date, "d")
                            logger(
                                "m2 reported fall percentage: " + str(self.seasonal_report_fall_percentage), "d")
                            logger("m2 reported fall color: " + self.seasonal_report_fall_color, "d")
                        except Exception as e:
                            logger("m2: try lvl 2 exception: " + str(e), "d")
                else:
                    try:
                        logger("m2_1: try lvl 1", "d")
                        m2_1 = m2_1[0]
                        logger("m2_1 reported name: " + str(m2_1), "d")
                        self.seasonal_report_fall_percentage = float(m2_1.reported_percentage)
                        self.seasonal_report_fall_color = str(m2_1.reported_percentage_color)
                        self.seasonal_report_fall_date = str(m2_1.reported_date)
                        logger("m2 reported fall date: " + self.seasonal_report_fall_date, "d")
                        logger("m2 reported fall percentage: " + str(self.seasonal_report_fall_percentage), "d")
                        logger("m2 reported fall color: " + self.seasonal_report_fall_color, "d")
                    except Exception as e:
                        logger("m2: try lvl 1 exception: " + str(e), "d")

                m3_1 = \
                    self.seasonal_report.filter(reported_date_year=m1.reported_date_year, reported_date_month='6')
                if m3_1.count() == 0:
                    logger("m3_1 is empty", "d")
                    m3_2 = \
                        self.seasonal_report.filter(reported_date_year=m1.reported_date_year, reported_date_month='5')
                    if m3_2.count() == 0:
                        logger("m3_2 is empty", "d")
                        m3_3 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year, reported_date_month='4')
                        if m3_3.count() == 0:
                            logger("m3_3 is empty", "d")
                        else:
                            try:
                                logger("m3_3: try lvl 3", "d")
                                m3_3 = m3_3[0]
                                logger("m3_3 reported name: " + str(m3_3), "d")
                                self.seasonal_report_summer_percentage = float(m3_3.reported_percentage)
                                self.seasonal_report_summer_color = str(m3_3.reported_percentage_color)
                                self.seasonal_report_summer_date = str(m3_3.reported_date)
                                logger("m3 reported summer date: " + self.seasonal_report_summer_date, "d")
                                logger(
                                    "m3 reported summer percentage: " + str(self.seasonal_report_summer_percentage), "d")
                                logger("m3 reported summer color: " + self.seasonal_report_summer_color, "d")
                            except:
                                pass
                    else:
                        try:
                            logger("m3_2: try lvl 2", "d")
                            m3_2 = m3_2[0]
                            logger("m3_2 reported name: " + str(m3_2), "d")
                            self.seasonal_report_summer_percentage = float(m3_2.reported_percentage)
                            self.seasonal_report_summer_color = str(m3_2.reported_percentage_color)
                            self.seasonal_report_summer_date = str(m3_2.reported_date)
                            logger("m3 reported summer date: " + self.seasonal_report_summer_date, "d")
                            logger("m3 reported summer percentage: " + str(self.seasonal_report_summer_percentage), "d")
                            logger("m3 reported summer color: " + self.seasonal_report_summer_color, "d")
                        except Exception as e:
                            logger("m3: try lvl 2 exception: " + str(e), "d")
                else:
                    try:
                        logger("m3_1: try lvl 1", "d")
                        m3_1 = m3_1[0]
                        logger("m3_1 reported name: " + str(m3_1), "d")
                        self.seasonal_report_summer_percentage = float(m3_1.reported_percentage)
                        self.seasonal_report_summer_color = str(m3_1.reported_percentage_color)
                        self.seasonal_report_summer_date = str(m3_1.reported_date)
                        logger("m3 reported summer date: " + self.seasonal_report_summer_date, "d")
                        logger("m3 reported summer percentage: " + str(self.seasonal_report_summer_percentage), "d")
                        logger("m3 reported summer color: " + self.seasonal_report_summer_color, "d")
                    except Exception as e:
                        logger("m3: try lvl 1 exception: " + str(e), "d")
                m4_1 = \
                    self.seasonal_report.filter(reported_date_year=m1.reported_date_year, reported_date_month='3')
                if m4_1.count() == 0:
                    logger("m4_1 is empty", "d")
                    m4_2 = \
                        self.seasonal_report.filter(reported_date_year=m1.reported_date_year, reported_date_month='2')
                    if m4_2.count() == 0:
                        logger("m4_2 is empty", "d")
                        m4_3 = self.seasonal_report.filter(reported_date_year=m1.reported_date_year, reported_date_month='1')
                        if m4_3.count() == 0:
                            logger("m4_3 is empty", "d")
                        else:
                            try:
                                logger("m4_3: try lvl 3", "d")
                                m4_3 = m4_3[0]
                                logger("m4_3 reported name: " + str(m4_3), "d")
                                self.seasonal_report_spring_percentage = float(m4_3.reported_percentage)
                                self.seasonal_report_spring_color = str(m4_3.reported_percentage_color)
                                self.seasonal_report_spring_date = str(m4_3.reported_date)
                                logger("m4 reported spring date: " + self.seasonal_report_spring_date, "d")
                                logger(
                                    "m4 reported spring percentage: " + str(self.seasonal_report_spring_percentage), "d")
                                logger("m4 reported spring color: " + self.seasonal_report_spring_color, "d")
                            except:
                                pass
                    else:
                        try:
                            logger("m4_2: try lvl 2", "d")
                            m4_2 = m4_2[0]
                            logger("m4_2 reported name: " + str(m4_2), "d")
                            self.seasonal_report_spring_percentage = float(m4_2.reported_percentage)
                            self.seasonal_report_spring_color = str(m4_2.reported_percentage_color)
                            self.seasonal_report_spring_date = str(m4_2.reported_date)
                            logger("m4 reported spring date: " + self.seasonal_report_spring_date, "d")
                            logger(
                                "m4 reported spring percentage: " + str(self.seasonal_report_spring_percentage), "d")
                            logger("m4 reported spring color: " + self.seasonal_report_spring_color, "d")
                        except Exception as e:
                            logger("m4: try lvl 2 exception: " + str(e), "d")
                else:
                    try:
                        logger("m4_1: try lvl 1", "d")
                        m4_1 = m4_1[0]
                        logger("m4_1 reported name: " + str(m4_1), "d")
                        self.seasonal_report_spring_percentage = float(m4_1.reported_percentage)
                        self.seasonal_report_spring_color = str(m4_1.reported_percentage_color)
                        self.seasonal_report_spring_date = str(m4_1.reported_date)
                        logger("m4 reported spring date: " + self.seasonal_report_spring_date, "d")
                        logger("m4 reported spring percentage: " + str(self.seasonal_report_spring_percentage), "d")
                        logger("m4 reported spring color: " + self.seasonal_report_spring_color, "d")
                    except Exception as e:
                        logger("m4: try lvl 1 exception: " + str(e), "d")
            logger("", "d")
            logger("------------------------ seasonal ready check start ----------------------", "d")
            if self.seasonal_report_spring_color != "None" and self.seasonal_report_spring_color is not None:
                logger("seasonal_report_spring_color: " + self.seasonal_report_spring_color, 'd')
                self.is_seasonal_report_ready = True
                logger("self.is_seasonal_report_ready: " + "True", "d")
            elif self.seasonal_report_summer_color != "None" and self.seasonal_report_summer_color is not None:
                logger("seasonal_report_summer_color: " + self.seasonal_report_summer_color, 'd')
                self.is_seasonal_report_ready = True
                logger("self.is_seasonal_report_ready: " + "True", "d")
            elif self.seasonal_report_fall_color != "None" and self.seasonal_report_fall_color is not None:
                logger("seasonal_report_fall_color: " + self.seasonal_report_fall_color, 'd')
                self.is_seasonal_report_ready = True
                logger("self.is_seasonal_report_ready: " + "True", "d")
            elif self.seasonal_report_winter_color != "None" and self.seasonal_report_winter_color is not None:
                logger("seasonal_report_winter_color: " + self.seasonal_report_winter_color, 'd')
                self.is_seasonal_report_ready = True
                logger("self.is_seasonal_report_ready: " + "True", "d")
            else:
                self.is_seasonal_report_ready = False
                logger("self.is_seasonal_report_ready: " + "False", "d")
            logger("------------------------ seasonal ready check end ----------------------", "d")
        except Exception as e:
            logger("seasonal report main exception: " + str(e), "d")
            pass
        super().save(*args, **kwargs)


class IsRobotRun(models.Model):
    idx = models.PositiveSmallIntegerField(null=False, blank=False, editable=False, unique=True, verbose_name='id')
    is_robot_run = models.BooleanField(default=False, editable=False, verbose_name="آیا ربات در حال اجراست؟")

    def __str__(self):
        return str(self.is_robot_run)

    class Meta:
        verbose_name = "robot check"
        verbose_name_plural = "robot checks"
