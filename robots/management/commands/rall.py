from django.core.management.base import BaseCommand
from companies.models import Companieslink
from logger.models import LoggerDebug, LoggerProduction
from robots.models import Company, MonthlyReport, SeasonalReport


def clear_data():
    Company.objects.all().delete()
    MonthlyReport.objects.all().delete()
    SeasonalReport.objects.all().delete()
    Companieslink.objects.all().delete()
    LoggerDebug.objects.all().delete()
    LoggerProduction.objects.all().delete()


class Command(BaseCommand):
    def handle(self, *args, **options):
        clear_data()
