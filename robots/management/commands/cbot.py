from django.core.management.base import BaseCommand
from robots.cbot import bot


class Command(BaseCommand):
    def handle(self, *args, **options):
        bot()
