from django.contrib import admin
from robots.models import Company, MonthlyReport, SeasonalReport, IsRobotRun


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ("badge", "name", "is_monthly_report_ready", "is_seasonal_report_ready", "created_at", "updated_at",)
    ordering = ("-updated_at", "id",)
    search_fields = ["badge", ]
    readonly_fields = (
        'monthly_report_1_date',
        'monthly_report_comparison_1_4',
        'monthly_report_2_date',
        'monthly_report_comparison_2_5',
        'monthly_report_3_date',
        'monthly_report_comparison_3_6',
        'seasonal_report_spring_date',
        'seasonal_report_spring_percentage',
        'seasonal_report_spring_color',
        'seasonal_report_summer_date',
        'seasonal_report_summer_percentage',
        'seasonal_report_summer_color',
        'seasonal_report_fall_date',
        'seasonal_report_fall_percentage',
        'seasonal_report_fall_color',
        'seasonal_report_winter_date',
        'seasonal_report_winter_percentage',
        'seasonal_report_winter_color',
        "is_monthly_report_ready",
        "is_seasonal_report_ready",
    )
    fields = (
        "badge",
        "name",
        "badge_link",
        "created_at",
        "updated_at",
        'monthly_report',
        'seasonal_report',
        'monthly_report_1_date',
        'monthly_report_comparison_1_4',
        'monthly_report_2_date',
        'monthly_report_comparison_2_5',
        'monthly_report_3_date',
        'monthly_report_comparison_3_6',
        'seasonal_report_spring_date',
        'seasonal_report_spring_percentage',
        'seasonal_report_spring_color',
        'seasonal_report_summer_date',
        'seasonal_report_summer_percentage',
        'seasonal_report_summer_color',
        'seasonal_report_fall_date',
        'seasonal_report_fall_percentage',
        'seasonal_report_fall_color',
        'seasonal_report_winter_date',
        'seasonal_report_winter_percentage',
        'seasonal_report_winter_color',
        "is_monthly_report_ready",
        "is_seasonal_report_ready",
    )
    list_filter = ('is_monthly_report_ready', 'is_seasonal_report_ready',)


@admin.register(MonthlyReport)
class MonthlyReportAdmin(admin.ModelAdmin):
    list_display = (
    "name", "company_name", "reported_date", "is_finished", "is_acceptable", "created_at", "updated_at",)
    ordering = ("-id", "-reported_date",)
    search_fields = ["company_name", ]


@admin.register(SeasonalReport)
class SeasonalReportAdmin(admin.ModelAdmin):
    list_display = (
    "name", "company_name", "reported_date", "is_finished", "is_acceptable", "created_at", "updated_at",)
    ordering = ("-id", "-reported_date",)
    search_fields = ["company_name", ]

@admin.register(IsRobotRun)
class IsRobotRunAdmin(admin.ModelAdmin):
    list_display = ('is_robot_run', )