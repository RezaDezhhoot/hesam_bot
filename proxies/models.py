import requests
from django.db import models
from latest_user_agents import get_random_user_agent
from companies.models import Companieslink
from config_setting.models import ConfigSetting
from robots.models import Company, MonthlyReport, SeasonalReport
import time
from logger.views import logger


class Proxy(models.Model):
    address = models.CharField(max_length=30, null=False, blank=False, verbose_name="proxy address")
    ip = models.CharField(max_length=30, null=False, blank=False, verbose_name="proxy ip")
    is_working = models.BooleanField(default=False, editable=False, verbose_name="آیا پراکسی فعال است؟")
    response_text = models.TextField(default="-", editable=False, verbose_name="داده های دریافت شده توسط پراکسی")

    def __str__(self):
        return "proxy: " + self.address + ":" + self.ip

    class Meta:
        verbose_name = "پروکسی"
        verbose_name_plural = "پروکسی"

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        res = check_proxy_availability(get_proxy())
        if res[0]:
            self.is_working = True
        else:
            self.is_working = False
        self.response_text = res[1]
        super().save(*args, **kwargs)


def get_proxy():
    proxies = Proxy.objects.all().order_by('?')
    if proxies.count() == 0:
        px = {'http': 'none',
              'https': 'none'}
        return px
    px = {}
    for proxy in proxies:
        px = {'http': str(getattr(proxy, "address")) + ":" + str(getattr(proxy, "ip")),
              'https': str(getattr(proxy, "address")) + ":" + str(getattr(proxy, "ip"))}
    return px


headers = {
    'user-agent': get_random_user_agent(),
}


def check_proxy_availability(proxy_list):
    settings = ConfigSetting.objects.all()[0]
    if not settings.is_proxy_on:
        res = 'proxy is off'
        return True, res
    logger("checking proxy " + str(proxy_list) + "...", 'd')
    url = "https://www.codal.ir"
    try:
        logger("waiting 2 second", 'd')
        time.sleep(2)
        logger("start check proxy availability", 'd')
        r = requests.get(url, headers=headers, proxies=proxy_list, timeout=5)
        res = r.text.strip()
        logger("proxies are ok.", 'd')
        logger("waiting 3 second", 'd')
        time.sleep(3)
        return True, res
    except TimeoutError as e:
        res = "timeout"
        logger("check proxy availability: codal.ir loading problem" + str(e), 'd')
        logger("waiting 3 second", 'd')
        time.sleep(3)
        return False, res
    except Exception as e:
        res = "proxy exception"
        logger("check proxy availability: codal.ir loading problem" + str(e), 'd')
        logger("waiting 3 second", 'd')
        time.sleep(3)
        return False, res
