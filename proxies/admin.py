from django.contrib import admin
from proxies.models import Proxy


@admin.register(Proxy)
class ProxyAdmin(admin.ModelAdmin):
    list_display = (
        'address',
        'ip',
        'is_working',
    )
    readonly_fields = (
        'is_working',
        'response_text',
    )
    fields = (
        'address',
        'ip',
        'is_working',
        'response_text',
    )
