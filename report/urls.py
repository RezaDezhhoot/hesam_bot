from django.urls import path

from report.views import report_spring, report_winter, report_fall, report_summer

app_name = 'report'

urlpatterns = [
    path('3/', report_spring, name='spring'),
    path('6/', report_summer, name='summer'),
    path('9/', report_fall, name='fall'),
    path('12/', report_winter, name='winter'),
]
