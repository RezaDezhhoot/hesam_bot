# Generated by Django 4.0.6 on 2022-10-31 17:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('config_setting', '0002_alter_configsetting_options'),
    ]

    operations = [
        migrations.AddField(
            model_name='configsetting',
            name='is_proxy_on',
            field=models.BooleanField(default=True, verbose_name='پراکسی فعال باشد؟'),
        ),
    ]
