# Generated by Django 4.0.6 on 2022-09-08 13:42

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ConfigSetting',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sleep_time', models.PositiveSmallIntegerField(default=60, verbose_name='زمان توقف ربات بین هر کوئری')),
            ],
        ),
    ]
