from django.db import models


class ConfigSetting(models.Model):
    sleep_time = models.PositiveSmallIntegerField(default=60, verbose_name="زمان توقف ربات بین هر کوئری")
    is_proxy_on = models.BooleanField(default=True, null=False, blank=False, verbose_name='پراکسی فعال باشد؟')

    def __str__(self):
        return str(": زمان تعیین شده") + str(self.sleep_time)

    class Meta:
        verbose_name = "تنظیمات"
        verbose_name_plural = "تنظیمات"