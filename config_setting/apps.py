from django.apps import AppConfig


class ConfigSettingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'config_setting'
    verbose_name = 'تنظیمات'
