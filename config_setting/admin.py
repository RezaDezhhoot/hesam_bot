from django.contrib import admin
from config_setting.models import ConfigSetting


@admin.register(ConfigSetting)
class Admin(admin.ModelAdmin):
    list_display = (
        'sleep_time',
        'is_proxy_on',
    )

    fields = (
        'sleep_time',
        'is_proxy_on',
    )

    def has_add_permission(self, request):
        if self.model.objects.count() >= 1:
            return False
        return super().has_add_permission(request)
